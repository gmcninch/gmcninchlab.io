---
author: George McNinch
title: GMCDs
date: 2024-01-07 18:54
header-includes:  \usepackage{palatino,mathpazo}
---

# gmcd-2023-12 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/214fzxCbhhtXsXoKRuTlMW?si=2358ca80db2c4370) 
  Playlist created: 2023-12-25   
 
  #. Black Pumas - *Ain't No Love In The Heart Of The City*     
     From the album [Deluxe Edition](https://blackpumas.bandcamp.com/album/deluxe-edition) 

  #. Guts - *Medewui ft. Pat Kalla & Assane Mboup*     
     From the album [Estrellas](https://guts-puravida.bandcamp.com/album/estrellas) 

  #. Kutiman - *My Everything (Featuring Dekel)*     
     From the album [Kutiman Open](https://kutiman.bandcamp.com/album/kutiman-open) 

  #. Fatima - *Westside*     
     From the album [And Yet It's All Love](https://fatima.bandcamp.com/album/and-yet-its-all-love) 

  #. Hidden Orchestra - *Hammered*     
     From the album [To Dream is to Forget](https://hiddenorchestra.bandcamp.com/album/to-dream-is-to-forget) 

  #. Jazzanova - *Scorpio's Child*     
     From the album [Strata Records The Sound of Detroit (Reimagined By Jazzanova)](https://jazzanova.bandcamp.com/album/strata-records-the-sound-of-detroit-reimagined-by-jazzanova) 

  #. Monophonics - *The Shape Of My Teardrops*     
     From the album [Sage Motel](https://monophonics.bandcamp.com/album/sage-motel) 

  #. Nostalgia 77 - *Flower*     
     From the album [The Loneliest Flower in the Village](https://nostalgia77.bandcamp.com/album/the-loneliest-flower-in-the-village) 

  #. Radio Citizen - *Seashores Of The Eye*     
     From the album [Lost & Found](https://radiocitizen.bandcamp.com/album/lost-found) 

  #. Black Pumas - *Mrs. Postman*     
     From the album [Chronicles of a Diamond](https://blackpumas.bandcamp.com/album/chronicles-of-a-diamond) 

  #. De La Soul - *Stakes Is High*     
     From the album **Stakes Is High** 


# gmcd-2022-12 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/5rpLrHJuP9IxDwrxaZY1fY?si=a93cc754e0064e91) 
  Playlist created: 2022-12-22   
 
  #. Tiawa - *Mountains Of Metal*     
     From the album [Moonlit Train](https://tiawa-music.bandcamp.com/album/moonlit-train) 

  #. Ana Mazzotti - *Agora Ou Nunca Mais*     
     From the album [Ninguem Vai Me Segurar](https://anamazzotti.bandcamp.com/album/ninguem-vai-me-segurar) 

  #. Gretchen Parlato - *É Preciso Perdoar*     
     From the album [Flor](https://gretchenparlato.bandcamp.com/album/flor) 

  #. Cleo Sol - *Sunshine*     
     From the album [Mother](https://cleosol.bandcamp.com/album/rose-in-the-dark) 

  #. Roy Ayers with Adrian Younge and Ali Shaheed Muhammad - *Sunflowers*     
     From the album [Roy Ayers JID002](https://royayers.bandcamp.com/album/roy-ayers-jid002) 

  #. Submotion Orchestra - *All Yours*     
     From the album [Finest Hour](https://submotionorchestra.bandcamp.com/album/finest-hour) 

  #. The Souljazz Orchestra - *Sorrow Fly Away*     
     From the album [Under Burning Skies](https://thesouljazzorchestra.bandcamp.com/album/under-burning-skies) 

  #. The Dining Rooms - *Art Is A Cat (Feat. Beatrice Velasco Moreno)*     
     From the album [Art Is A Cat](https://thediningrooms.bandcamp.com/album/art-is-a-cat) 

  #. Beady Belle - *Last Dance*     
     From the album [Nothing But The Truth](https://beadybelle.bandcamp.com/album/nothing-but-the-truth) 

  #. Guts - *Já Não Há Mais Paz Feat. Catia Werneck*     
     From the album [Philantropiques](https://guts-puravida.bandcamp.com/album/philantropiques) 

  #. Natalie Duncan - *Sirens*     
     From the album [Free](https://natalieduncan.bandcamp.com/album/free) 

  #. Tiawa - *Can't Turn Back*     
     From the album [Moonlit Train](https://tiawa-music.bandcamp.com/album/moonlit-train) 


# gmcd-2020-12 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/1f4dWsZvFHY2aYZawJydLF?si=c579d857ca4142af) 
  Playlist created: 2020-12-24   
 
  #. Pete Josef - *Move On*     
     From the album [Colour](https://petejosef.bandcamp.com/album/colour) 

  #. The Pharaohs - *Somebody's Been Sleeping*     
     From the album [Awakening](https://pharaohschicago.bandcamp.com/album/awakening) 

  #. Quantic - *Orquídea feat. Sly5thAve*     
     From the album [Atlantic Oscillations](https://quanticmusic.bandcamp.com/album/atlantic-oscillations-2) 

  #. Sufjan Stevens - *The Ascension*     
     From the album [The Ascension](https://music.sufjan.com/album/the-ascension) 

  #. Bryony Jarman-Pinto - *Day Dream (Fish Factory Session)*     
     From the album [Fish Factory Sessions EP](https://bryonyjarman-pinto.bandcamp.com/album/fish-factory-sessions-ep) 

  #. S-Tone Inc - *Odoya (featuring Toco)*     
     From the album [Body & Soul](https://s-toneinc.bandcamp.com/album/body-soul) 

  #. Sufjan Stevens - *Death Star*     
     From the album [The Ascension](https://music.sufjan.com/album/the-ascension) 

  #. Pete Josef - *The Travelling Song*     
     From the album [Colour](https://petejosef.bandcamp.com/album/colour) 

  #. Sam Rivers - *Beatrice*     
     From the album **Fuchsia Swing Song** 

  #. Lana Del Rey - *Happiness Is A Butterfly*     
     From the album [NFR! Norman Fucking Rockwell](https://www.amazon.com/NFR-Lana-Del-Rey/dp/B07VBH59H1) 

  #. S-Tone Inc - *In The Name Of Love (featuring Laura Fedele)*     
     From the album [Body & Soul](https://s-toneinc.bandcamp.com/album/body-soul) 

  #. Norah Jones - *Man Of The Hour*     
     From the album [The Fall](https://www.amazon.com/Fall-Norah-Jones/dp/B002NWRMVS/) 


# gmcd-2020-06 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/2KRW5tfrejbyBQb440fOxr?si=e74eaf5a69b647e2) 
  Playlist created: 2020-06-24   
 
  #. Meow Meow, Thomas Lauderdale, and Pink Martini - *Hotel Amour*     
     From the album [Hotel Amour](https://pinkmartini.bandcamp.com/album/hotel-amour) 

  #. Quantic - *Politik Society Feat Noelle Scaggs*     
     From the album [An Announcement to Answer](https://quanticmusic.bandcamp.com/album/an-announcement-to-answer) 

  #. Jad & The - *Underwater feat. Mark Borgazzi*     
     From the multi-artist album [Sonar Kollekting Vol. 02](https://sonarkollektiv.bandcamp.com/album/sonar-kollekting-vol-2) 

  #. Alison Crockett - *O Cantador*     
     From the album [Obrigada](https://alisoncrockett.bandcamp.com/album/obrigada) 

  #. Pete Josef - *Something Good*     
     From the album [Colour](https://petejosef.bandcamp.com/album/colour) 

  #. Luciana Souza - *Se Acontecer*     
     From the album [Storytellers](https://lucianasouza.bandcamp.com/album/storytellers) 

  #. Radio Citizen - *Rise*     
     From the album [Live At The Kantine Am Berghain](https://radiocitizen.bandcamp.com/album/live-at-the-kantine-am-berghain) 

  #. Key Elements - *Orchestra Mode feat. Gerry Franke*     
     From the album [Key Elements](https://keyelementssk.bandcamp.com/album/key-elements) 

  #. John Scofield - *Icons at the Fair*     
     From the album [Combo 66](http://www.johnscofield.com/discography/) 

  #. Carmen Lundy - *Burden Down Burden Down*     
     From the album [Modern Ancestors](https://carmenlundy.com/product/modern-ancestors/) 

  #. Beady Belle - *Traces*     
     From the album [Dedication](https://www.beadybelle.com/discography) 

  #. Jazzanova - *Summer Keeps On Passing Me By feat. Ben Westbeech*     
     From the album [The Pool](https://jazzanova.bandcamp.com/album/the-pool) 

  #. Nostalgia 77 - *Quiet Dawn feat. Beth Rowley*     
     From the multi-artist album [Fifteen (Best Of Nostalgia 77)](https://nostalgia77.bandcamp.com/album/fifteen-best-of) 

  #. Edna Vazquez with Pink Martini - *Sola Soy*     
     From the album [Bésame Mucho](http://www.ednavazquez.com/album/besame-mucho/) 


# gmcd-2019-12 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/1MPgxz8pgSRGD2TbOBxJ01?si=b0ac312ffffe44ce) 
  Playlist created: 2019-12-26   
 
  #. Angelique Kidjo - *Born Under Punches (The Heat Goes On)*     
     From the album [Remain in Light](https://www.amazon.com/Remain-Light-ANGELIQUE-KIDJO/dp/B07GW4FD48/) 

  #. Nicola Conte & Stefania Dipierro - *A Gira*     
     From the album [Natural](https://nicolacontestefaniadipierro.bandcamp.com/album/natural-2) 

  #. Edna Vazquez with Pink Martini - *Bésame Mucho*     
     From the album [Bésame Mucho](http://www.ednavazquez.com/album/besame-mucho/) 

  #. The Cinematic Orchestra - *A Caged Bird/Imitations of Life (feat. Roots Manuva)*     
     From the album [To Believe](https://cinematicorchestramusic.bandcamp.com/album/to-believe) 

  #. David Bowie - *Lazarus*     
     From the album [Blackstar](https://www.amazon.com/Blackstar-David-Bowie/dp/B017VORJK6/) 

  #. Ryan Keberle & Catharsis - *Gallop*     
     From the album [Into the Zone](https://ryankeberle.bandcamp.com/album/into-the-zone) 

  #. Jamie Baum - *Nusrat*     
     From the album [In this life](https://jamiebaum.bandcamp.com/album/in-this-life) 

  #. Bryony Jarman-Pinto - *Saffron Yellow*     
     From the album [Cage & Aviary](https://bryonyjarman-pinto.bandcamp.com/album/cage-aviary) 

  #. Norma Winstone - *The mermaid*     
     From the album [Distances](https://www.amazon.com/Distances-Norma-Winstone/dp/B0012NON7U/) 

  #. Patricia Barber - *High Summer Season*     
     From the album [Higher](https://www.amazon.com/Higher-Patricia-Barber/dp/B07PQTDXC3/) 

  #. Elizabeth Shepherd - *La boxe*     
     From the album [Montréal](https://elizabethshepherd.bandcamp.com/album/montr-al) 

  #. Benny Sings - *Everything I Know*     
     From the album [City Pop](https://bennysings.bandcamp.com/album/city-pop) 

  #. Cat Power - *You Get*     
     From the album [Wanderer](https://www.amazon.com/Wanderer-Cat-Power/dp/B07FDN2VFB/) 

  #. Camila Meza & The Nectar Orchestra - *This is Not America*     
     From the album [Ámbar](https://www.amazon.com/Ambar-Camila-Meza/dp/B07PFLFLWY/) 


# gmcd-2018-12 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/71G3mmIPPpjLBMSzHRr8GK?si=d4609726d5a945b0) 
  Playlist created: 2018-12-20   
 
  #. Jazzanova - *Slow Rise feat. David Lemaitre*     
     From the album [The Pool](https://jazzanova.bandcamp.com/album/the-pool) 

  #. John Coltrane - *Vilia*     
     From the album [Both Directions At Once (The Lost Album)](https://www.amazon.com/Both-Directions-At-Once-Album/dp/B07D4ZWCHX/) 

  #. Slowly Rolling Camera - *The Outlier*     
     From the album [Juniper](https://slowlyrollingcamera.bandcamp.com/album/juniper) 

  #. Radio Citizen - *Did You Waltz*     
     From the album [Warm Canto EP](https://radiocitizen.bandcamp.com/album/warm-canto-ep) 

  #. Norma Winstone - *Joy Spring*     
     From the album [Well Kept Secret](https://normawinstone.bandcamp.com/album/well-kept-secret) 

  #. Chip Wickham - *Snake Eyes*     
     From the album [Shamal Wind](https://chipwickham.bandcamp.com/album/shamal-wind) 

  #. Luciana Souza - *Paris*     
     From the album [The Book of Longing](https://lucianasouza.bandcamp.com/album/the-book-of-longing) 

  #. Jazzanova - *Let's Live Well feat. Jamie Cullum*     
     From the album [The Pool](https://jazzanova.bandcamp.com/album/the-pool) 

  #. Jukka Eskola - *And The Day Passed By*     
     From the album [Orquesta Bossa](https://jukkaeskola.bandcamp.com/album/orquesta-bossa) 

  #. Nostalgia 77 - *Rain Walk*     
     From the multi-artist album [Fifteen (Best Of Nostalgia 77)](https://nostalgia77.bandcamp.com/album/fifteen-best-of) 

  #. Ryan Keberle & Catharsis - *Fool on the Hill*     
     From the album [Find The Common, Shine A Light](https://ryankeberle.bandcamp.com/album/find-the-common-shine-a-light) 

  #. Matthew Halsall & The Gondwana Orchestra - *Into Forever Feat. Josephine Oniyama (Barck & Comixxx Remix)*     
     From the multi-artist album [Sonar Kollekting Vol. 02](https://sonarkollektiv.bandcamp.com/album/sonar-kollekting-vol-2) 

  #. Lee Morgan - *Melancholee*     
     From the album **Search for the New Land** 


# gmcd-2017-12 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/2BnRaGM2ieUKVb7uFmlYhh?si=a412a9d6170f49e8) 
  Playlist created: 2017-12-20   
 
  #. Radio Citizen - *Test Me (feat Ursula Rucker)*     
     From the album [Hope and Despair](https://radiocitizenubiq.bandcamp.com/album/hope-and-despair) 

  #. Build An Ark - *Dawn*     
     From the album [Dawn](https://www.amazon.com/Dawn-Build-Ark/dp/B000W352IU) 

  #. Luciana Souza - *Circus Life*     
     From the album [Tide](https://www.discogs.com/release/6271706-Luciana-Souza-Tide) 

  #. Recloose - *Deeper Waters feat. Joe Dukie (Eva Be Secret Lover Remix)*     
     From the multi-artist album [Secret Love 5](https://sonarkollektiv.bandcamp.com/album/secret-love-5-compiled-by-jazzanova) 

  #. Pink Martini - *Ich Dich Liebe*     
     From the album [Get Happy](https://pinkmartini.bandcamp.com/album/get-happy) 

  #. Bïa - *Bésame Mucho*     
     From the album [Navegar](https://www.amazon.com/Navegar-BIA/dp/B00RDEXM7E/) 

  #. Radio Citizen - *Ninguem*     
     From the album [Silent Guide](https://radiocitizensk.bandcamp.com/album/silent-guide) 

  #. Buck Clarke Sound - *Night in Tunisia*     
     From the multi-artist album [The Basic Principles Of Sound 2](https://www.discogs.com/Various-The-Basic-Principles-Of-Sound-Music-For-The-Modern-Listener-Volume-2/release/1208823) 

  #. Part Time Heroes - *Angels Fly featuring Jono Mccleery*     
     From the album [Meanwhile](https://part-timeheroes.bandcamp.com/album/meanwhile) 

  #. Radio Citizen - *Silent Guide*     
     From the album [Silent Guide](https://radiocitizensk.bandcamp.com/album/silent-guide) 

  #. Gerardo Frisina - *Will You Walk a Little Faster? (feat. Norma Winstone)*     
     From the album [Join the Dance](https://www.discogs.com/Gerardo-Frisina-Join-The-Dance/release/3049338) 

  #. Bïa - *Eleanor Rigby*     
     From the album [Navegar](https://www.amazon.com/Navegar-BIA/dp/B00RDEXM7E/) 


# gmcd-2017-10 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/0jehPbdbAiVoKHbn108YuS?si=98a5d025d8804eef) 
  Playlist created: 2017-10-30   
 
  #. Quantic - *Que Me Duele?*     
     From the album [Curao](https://quanticmusic.bandcamp.com/album/curao) 

  #. Teresa Cristina - *Meu Mundo e Hoje (Eu Sou Assim)*     
     From the multi-artist album [Putumayo Presents Acoustic Brasil](https://www.allmusic.com/album/putumayo-presents-acoustic-brazil-mw0000139039) 

  #. Hidden Orchestra - *Spoken*     
     From the album [Archipelago](https://hiddenorchestra.bandcamp.com/album/archipelago) 

  #. Liz Aku - *Just What I Need*     
     From the album [Ankhor](https://lizaku.bandcamp.com/album/ankhor) 

  #. Benny Sings - *One of These Hearts*     
     From the album [STUDIO](https://jakartarecords-label.bandcamp.com/album/studio) 

  #. Sabrina Malheiros - *Em Paz*     
     From the album [Clareia](https://sabrinamalheiros.bandcamp.com/album/clareia) 

  #. Quantic - *Amor en Francia*     
     From the album [Curao](https://quanticmusic.bandcamp.com/album/curao) 

  #. Les Escrocs - *Assedic*     
     From the multi-artist album [Putumayo Acoustic France](https://www.amazon.com/Acoustic-France-Putumayo-Presents/dp/B001B41SSM) 

  #. Sabrina Malheiros - *Vai, Maria*     
     From the album [Clareia](https://sabrinamalheiros.bandcamp.com/album/clareia) 

  #. Benny Sings - *The Beach House*     
     From the album [STUDIO](https://jakartarecords-label.bandcamp.com/album/studio) 

  #. Liz Aku - *The Drum Major Instinct feat. Mara TK*     
     From the album [Ankhor](https://lizaku.bandcamp.com/album/ankhor) 


# gmcd-2017-03 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/6yumJXDpSKaxCSxnPGQW2f?si=a05ec837c9c94856) 
  Playlist created: 2017-03-01   
 
  #. Joyce Elaine Yuille - *Come With Me*     
     From the album [Welcome To My World](https://joyceelaineyuille.bandcamp.com/album/welcome-to-my-world) 

  #. Holly Cole - *Little Boy Blue*     
     From the album [Temptation](https://www.amazon.com/Temptation-Holly-Cole/dp/B000SZBQ3M/) 

  #. The Dining Rooms - *Appuntamento Su Marte*     
     From the album [Do Hipsters Love Sun (Ra)](https://thediningrooms.bandcamp.com/album/do-hipsters-love-sun-ra-2) 

  #. Lucas Santtana - *Dia De Furar Onda No Mar*     
     From the multi-artist album [Paz E Futebol 2 (Compiled by Jazzanova)](https://jazzanova.bandcamp.com/album/paz-e-futebol-2-compiled-by-jazzanova) 

  #. Marisa Monte - *Dizem Que O Amor*     
     From the album [Coleção](https://www.amazon.com/Cole%C3%A7%C3%A3o-Marisa-Monte/dp/B073JZJY2Y/) 

  #. Os Originais Do Samba - *Canto Chorado*     
     From the multi-artist album [Paz E Futebol 2 (Compiled by Jazzanova)](https://jazzanova.bandcamp.com/album/paz-e-futebol-2-compiled-by-jazzanova) 

  #. The Dining Rooms - *Sergio Leone Asteroid*     
     From the album [Do Hipsters Love Sun (Ra)](https://thediningrooms.bandcamp.com/album/do-hipsters-love-sun-ra-2) 

  #. Joyce Elaine Yuille - *Chaos*     
     From the album [Welcome To My World](https://joyceelaineyuille.bandcamp.com/album/welcome-to-my-world) 

  #. Marisa Monte - *Waters Of March*     
     From the album [Coleção](https://www.amazon.com/Cole%C3%A7%C3%A3o-Marisa-Monte/dp/B073JZJY2Y/) 


# gmcd-2016-12 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/7emFiwQRMjS2dXzz9gCVkT?si=9d7a1560f6b942dc) 
  Playlist created: 2016-12-21   
 
  #. Medeski Martin & Wood - *Mami Gato*     
     From the album [Note Bleu (Best Of The Blue Note Years 1998-2005)](https://www.amazon.com/Note-Bleu-Best-Years-1998-2005/dp/B000EGDN04) 

  #. Luísa Maita - *Lero-Lero*     
     From the album [Lero-Lero](https://luisamaita.bandcamp.com/album/lero-lero) 

  #. Paulinho da Viola - *Dança da solidão*     
     From the album [Argumento](http://www.paulinhodaviola.com.br/english/index.htm) 

  #. Beady Belle - *The clouds*     
     From the album [On My Own](https://www.beadybelle.com/discography) 

  #. John Scofield - *Mr. Puffy*     
     From the album [Past Present](http://www.johnscofield.com/discography/) 

  #. Quantic Presenta Flowering Inferno - *Chambacu feat. Nidia Gongora*     
     From the album [1000 Watts](https://quanticmusic.bandcamp.com/album/1000-watts) 

  #. Dave Douglas - *Blue Heaven*     
     From the album [Soul On Soul](https://www.amazon.com/Soul-Dave-Douglas/dp/B000040JEO) 

  #. Camila Meza - *Traces*     
     From the album [Traces](https://camilameza.bandcamp.com/album/traces) 

  #. Partideiros Do Cacique - *Meu Bloco*     
     From the multi-artist album [The Rough Guide To Samba](https://www.amazon.com/Rough-Guide-Samba-Second/dp/B00WZOT2J4/) 

  #. Christian Prommer's Drumlesson - *Pure And Easy*     
     From the album [The Jazz Thing](https://thediningrooms.bandcamp.com/album/the-jazz-thing) 

  #. Luísa Maita - *Descencabulada*     
     From the album [Lero-Lero](https://luisamaita.bandcamp.com/album/lero-lero) 

  #. Till Brönner - *River Man feat. Till Brönner*     
     From the album [Oceana](https://www.amazon.com/Oceana-New-TILL-BROENNER/dp/B000HWZB1E/) 

  #. Beady Belle - *Bury*     
     From the album [On My Own](https://www.beadybelle.com/discography) 

  #. Hidden Orchestra - *Fourth Wall*     
     From the album [Archipelago](https://hiddenorchestra.bandcamp.com/album/archipelago) 

  #. Gerardo Frisina - *Gosto De Que E Bom*     
     From the multi-artist album [A History of Schema (Tokyo's Dream)](https://www.amazon.com/History-Schema-Tokyofs-compiles-H-Yamazaki/dp/B000VKNMOA) 

  #. Paulinho da Viola - *Argumento*     
     From the album [Argumento](http://www.paulinhodaviola.com.br/english/index.htm) 

  #. Quantic and his Combo Barbaro - *I Just Fell In Love Again*     
     From the album [Tradition In Transition](https://quanticmusic.bandcamp.com/album/tradition-in-transition) 

  #. Charlie Hunter Quartet - *Lively Up Yourself*     
     From the album [Natty Dread](https://www.amazon.com/Natty-Dread-Charlie-Hunter/dp/B000UFYUJM/) 


# gmcd-2015-12 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/5s8uoHr2kFy9IvQj1OQNhJ?si=c61aa2998fe645a6) 
  Playlist created: 2015-12-20   
 
  #. Roy Ayers - *Aragon*     
     From the album ['Coffy' Original Motion Picture Soundtrack](https://www.discogs.com/master/127408-Roy-Ayers-Coffy) 

  #. Elvis Costello And The Roots - *Wake Me Up*     
     From the album **Wise Up Ghost And Other Songs** 

  #. Zap Mama - *Rafiki*     
     From the album [A Ma Zone](https://zapmama.bandcamp.com/album/a-ma-zone) 

  #. Azymuth - *Chameleon*     
     From the album **Pure (Best of Far Out)** 

  #. José James - *What A Little Moonlight Can Do*     
     From the album **Yesterday I Had The Blues (The Music Of Billie Holiday)** 

  #. Sara Mitra - *Baltimore Oriole*     
     From the album [Losing You](https://impossiblearkrecords.bandcamp.com/album/losing-you) 

  #. Clarinet Factory - *5 Steps (Hidden Orchestra Remix)*     
     From the multi-artist album [Reorchestrations (Hidden Orchestra)](https://hidden-orchestra.bandcamp.com/album/reorchestrations) 

  #. Miguel Zenón - *Juguete*     
     From the album **Alma Adentro** 

  #. Radio Citizen - *Shores*     
     From the album [The Night & The City](https://radiocitizensk.bandcamp.com/album/the-night-the-city) 

  #. Luciana Souza - *At The Fair*     
     From the album [Speaking In Tongues](https://lucianasouza.bandcamp.com/album/speaking-in-tongues) 

  #. Bïa - *Beijo*     
     From the album [Navegar](https://www.amazon.com/Navegar-BIA/dp/B00RDEXM7E/) 

  #. Kenny Burrell - *Midnight Blue*     
     From the album **Midnight Blue** 

  #. Luciana Souza - *A Pebble In Still Water*     
     From the album [Speaking In Tongues](https://lucianasouza.bandcamp.com/album/speaking-in-tongues) 

  #. Long Arm - *Sleep Key (Hidden Orchestra Remix)*     
     From the multi-artist album [Reorchestrations (Hidden Orchestra)](https://hidden-orchestra.bandcamp.com/album/reorchestrations) 

  #. Bïa - *Navegar*     
     From the album [Navegar](https://www.amazon.com/Navegar-BIA/dp/B00RDEXM7E/) 

  #. Zap Mama - *Gissié*     
     From the album [A Ma Zone](https://zapmama.bandcamp.com/album/a-ma-zone) 

  #. Radio Citizen - *Phone*     
     From the album [The Night & The City](https://radiocitizensk.bandcamp.com/album/the-night-the-city) 


# gmcd-2014-12 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/3sdGooBf6mONDJvHWNsl1Y?si=1fde60e853fa43ea) 
  Playlist created: 2014-12-20   
 
  #. Quantic - *La Plata [feat. Nidia Góngora]*     
     From the album [Magnetica](https://quanticmusic.bandcamp.com/album/magnetica) 

  #. Chris Potter - *Shadow*     
     From the album **Gratitude** 

  #. Michael Kiwanuka - *Home again*     
     From the album **Home Again** 

  #. Luciana Souza - *Never Die Young*     
     From the album [The New Bossa Nova](https://www.discogs.com/master/1086384-Luciana-Souza-The-New-Bossa-Nova) 

  #. Marisa Monte - *Descalço no Parque*     
     From the album **Verdade Uma Ilusão** 

  #. Sonar Kollektiv Orchester - *Boom Clicky Boom Klack*     
     From the album [Guaranteed Niceness](https://sonarkollektiv.bandcamp.com/album/guaranteed-niceness) 

  #. Fatima - *La Neta*     
     From the album [Yellow Memories](https://www.discogs.com/release/5681809-Fatima-Yellow-Memories) 

  #. Beady Belle - *Poppy Burt-Jones*     
     From the album [Cricklewood Broadway](https://www.beadybelle.com/discography) 

  #. Bonobo - *Towers (feat. Szjerdene)*     
     From the album [The North Borders](https://bonobomusic.bandcamp.com/album/the-north-borders-2) 

  #. Snarky Puppy - *Thing Of Gold*     
     From the album **groundUP** 

  #. Gaetano Partipilo - *Atras da Porta (feat. Rosalia De Souza)*     
     From the album **Besides (Songs from the Sixties)** 

  #. Carmen Lundy - *Sardegna*     
     From the album **Soul to Soul** 

  #. Luciana Souza - *Tide*     
     From the album [Tide](https://www.discogs.com/release/6271706-Luciana-Souza-Tide) 

  #. Fatima - *Biggest Joke Of All*     
     From the album [Yellow Memories](https://www.discogs.com/release/5681809-Fatima-Yellow-Memories) 

  #. Bïa - *Les Mures Sauvages*     
     From the album **Sources** 

  #. Michael Kiwanuka - *Tell me a tale*     
     From the album **Home Again** 

  #. Lee Fields & The Expressions - *Moonlight Mile*     
     From the album **Faithful Man** 


# gmcd-2013-12-disk-2 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/4kP8ojMs2oBvjhEBh0dDow?si=2f892f9233bf4803) 
  Playlist created: 2013-12-21   
 
  #. Ray Charles - *Night Time Is The Right Time*     
     From the album **The Best Of Ray Charles (The Atlantic Years)** 

  #. Sufjan Stevens - *Concerning The UFO Sighting Near Highland, Illinois*     
     From the album **Illinois** 

  #. Joni Mitchell - *Furry Sings The Blues*     
     From the album **Hejira** 

  #. Patricia Barber - *Redshift*     
     From the album **Smash** 

  #. Myra Melford with Dave Douglas - *Bound Unbound*     
     From the album **The Same River, Twice** 

  #. Babelfish - *Sometime*     
     From the album **Babelfish** 

  #. Marisa Monte - *Descalço No Parque*     
     From the album **O Que Você Quer Saber De Verdade** 

  #. Luciana Souza - *Chega de Saudade*     
     From the album [North and South](https://lucianasouza.bandcamp.com/album/north-and-south) 

  #. Bïa - *Je n'aime pas*     
     From the album **Carmin** 

  #. Michael Brecker - *Don't Let Me Be Lonely Tonight*     
     From the album **Nearness Of You (The Ballad Book)** 

  #. Miguel Zenón - *Juguete*     
     From the album **Alma Adentro** 

  #. Joe Pass and Paulinho da Costa - *Barquinho*     
     From the album **Tudo Bem** 

  #. Stacey Kent - *La Venus du melo*     
     From the album **Raconte-moi...** 

  #. Marisa Monte - *Ainda Bem*     
     From the album **O Que Você Quer Saber De Verdade** 

  #. Luciana Souza - *The Thrill is Gone*     
     From the album [The Book of Chet](https://lucianasouza.bandcamp.com/album/the-book-of-chet) 

  #. Michael Brecker - *Sometimes I See*     
     From the album **Nearness Of You (The Ballad Book)** 

  #. Babelfish - *Popular Mechanics*     
     From the album **Babelfish** 

  #. Tom Waits - *Talking at the Same Time*     
     From the album **Bad as Me** 


# gmcd-2013-12-disk-1 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/1zAG6YHbCCKseGanhgSAhj?si=4f22366b13fa4da8) 
  Playlist created: 2013-12-21   
 
  #. Steely Dan - *Bad Sneakers*     
     From the album **Katy Lied** 

  #. The Budos Band - *Black Venom*     
     From the album **The Budos Band III** 

  #. Myra Melford's Be Bread - *I See a Horizon*     
     From the album **The Whole Tree Gone** 

  #. Joni Mitchell - *Court And Spark*     
     From the album **Court And Spark** 

  #. Patricia Barber - *Smash*     
     From the album **Smash** 

  #. Gary Burton (The New Gary Burton Quartet) - *Last Snow*     
     From the album **Common Ground** 

  #. Bïa - *Sabio rei*     
     From the album **Carmin** 

  #. Dee Alexander - *Wild Is The Wind*     
     From the album **Wild Is The Wind** 

  #. Babelfish - *Falando De Amor*     
     From the album **Babelfish** 

  #. Bill McHenry - *La Fuerza*     
     From the album **Ghosts of the Sun** 

  #. Bïa - *Mariana*     
     From the album **Carmin** 

  #. Marisa Monte - *O Que Se Quer*     
     From the album **O Que Você Quer Saber De Verdade** 

  #. Joe Pass and Paulinho da Costa - *Luciana*     
     From the album **Tudo Bem** 

  #. Luciana Souza - *Were You Blind That Day*     
     From the album [The New Bossa Nova](https://www.discogs.com/master/1086384-Luciana-Souza-The-New-Bossa-Nova) 

  #. Stacey Kent - *Les eaux de mars*     
     From the album **Raconte-moi...** 

  #. Marcos Valle - *Maria Mariana*     
     From the album **Escape** 

  #. Gil Scott-Heron - *Who'll Pay Reparations on my Soul*     
     From the album **Evolution (and Flashback)** 


# gmcd-2012-12-disk-2 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/5uszDdAOGkabjMJHdcVLE5?si=fb24119834a04641) 
  Playlist created: 2012-12-24   
 
  #. Steely Dan - *Any Major Dude Will Tell You*     
     From the album **Pretzel Logic** 

  #. Dominique A - *La fin d'un monde*     
     From the multi-artist album **Le Pop 5** 

  #. Spanish Harlem Orchestra - *Bailadores*     
     From the album **Across 110th Street** 

  #. Omara Portuondo - *Mueve la cintura mulato*     
     From the album **Flor De Amor** 

  #. Bebo & Cigala - *Lágrimas negras*     
     From the album **Lágrimas Negras** 

  #. Luciana Souza - *Here It Is*     
     From the album [The New Bossa Nova](https://www.discogs.com/master/1086384-Luciana-Souza-The-New-Bossa-Nova) 

  #. Kate McGarry - *Charade*     
     From the album **Girl Talk** 

  #. Anat Cohen - *St James Infirmary*     
     From the album **Clarinetwork Live At The Village Vanguard** 

  #. JD Allen Trio - *The Matador And The Bull (Torero)*     
     From the album **The Matador and the Bull** 

  #. Fertile Ground - *Yellow Daisies*     
     From the album **Black Is....** 

  #. Betty Wright - *Baby Come Back (Feat. Lenny Williams)*     
     From the album **Betty Wright: The Movie** 

  #. Kelley Johnson - *Should've Been*     
     From the album **Home** 

  #. Sara Mitra - *Baby And Me*     
     From the album [April Song](https://impossiblearkrecords.bandcamp.com/album/april-song) 

  #. Micatone - *Break My Heart*     
     From the album [Wish I Was Here](https://micatone.bandcamp.com/album/wish-i-was-here) 

  #. Marisa Monte - *Bem Leve*     
     From the album [A Great Noise](https://www.discogs.com/release/770356-Marisa-Monte-A-Great-Noise) 

  #. El Perro Del Mar - *Change Of Heart*     
     From the multi-artist album **Secret Love 6** 

  #. Tom Waits - *New Year's Eve*     
     From the album **Bad as Me** 

  #. Gary McFarland - *Bloop Bleep*     
     From the album **The In Sound** 


# gmcd-2012-12-disk-1 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/2coWXhiqEhhFrp5cJVIB5t?si=d129ccc722a64ee6) 
  Playlist created: 2012-12-24   
 
  #. Aloe Blacc - *I Need a Dollar*     
     From the album **Good Things** 

  #. Betty Wright - *In The Middle Of The Game (Don't Change The Play)*     
     From the album **Betty Wright: The Movie** 

  #. Lee Fields & The Expressions - *It's All Over (But The Crying)*     
     From the album **Faithful Man** 

  #. Lizz Wright - *Stop*     
     From the album **Dreaming Wide Awake** 

  #. Kate McGarry - *It's a Wonderful World*     
     From the album **Girl Talk** 

  #. Bill Evans - *Sweet Dulcinea Blue*     
     From the album **Quintessence** 

  #. Mario Biondi and the High Five Quintet - *On A Clear Day (You Can See Forever)*     
     From the album **A Handful of Soul** 

  #. Sergio Mendes and Brasil '66 - *Mais Que Nada*     
     From the album **The Very Best** 

  #. Sara Mitra - *Jilted Woman Blues*     
     From the album [April Song](https://impossiblearkrecords.bandcamp.com/album/april-song) 

  #. Tim Knol - *Days*     
     From the multi-artist album **Secret Love 6** 

  #. Carmen Lundy - *The Night Is Young*     
     From the album **Changes** 

  #. Hidden Orchestra - *Tired And Awake*     
     From the album [Night Walks](https://hiddenorchestra.bandcamp.com/album/night-walks) 

  #. Lorez Alexandria - *That Old Devil Called Love*     
     From the album **For Swingers Only** 

  #. Kinny - *Lost Baggage*     
     From the album [Can't Kill A Dame With Soul](https://www.amazon.com/Cant-Kill-Dame-Soul-Kinny/dp/B005PKQV12/) 

  #. Jazzanova - *I Can See*     
     From the album **Funkhaus Studio Sessions** 

  #. Charles Bradley - *No Time For Dreaming*     
     From the album **No Time For Dreaming** 

  #. Marisa Monte - *Dança da solidão*     
     From the album **Rose and Charcoal** 

  #. Seu Jorge - *O Samba Tai*     
     From the album **Carolina** 


# gmcd-2011-12-disk-2 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/7Gmx5ZxAeMEBPQtjbIQpFM?si=4ef80153b76e417c) 
  Playlist created: 2011-12-22   
 
  #. The Horace Silver Quintet - *The Cape Verdean Blues*     
     From the album **The Cape Verdean Blues** 

  #. Chick Corea - *Armando's Rhumba (feat Bobby McFerrin)*     
     From the album **Rendezvous In New York** 

  #. Dave Pike - *Sunny*     
     From the album **Jazz For The Jet Set** 

  #. Elvis Costello and Allen Toussaint - *Nearer To You*     
     From the album **The River In Reverse** 

  #. Ann Peebles - *I Can't Stand The Rain*     
     From the album **I Can't Stand The Rain** 

  #. Nick Lowe - *Poisoned Rose*     
     From the album **The Old Magic** 

  #. Charles Bradley - *No Time For Dreaming*     
     From the album **No Time For Dreaming** 

  #. Betty Wright - *So Long, So Wrong*     
     From the album **Betty Wright: The Movie** 

  #. Ray Charles - *Drown In My Own Tears*     
     From the album **The Best Of Ray Charles (The Atlantic Years)** 

  #. Baker Brothers - *Family Tree featuring Vanessa Freeman*     
     From the album **Avid Sounds** 

  #. Nicola Conte - *The nubian queens*     
     From the album **Rituals** 

  #. Lizz Wright - *Leave Me Standing Alone*     
     From the album **The Orchard** 

  #. Nicole Willis & The Soul Investigators - *Blues Downtown*     
     From the album **Keep Reachin' Up** 

  #. Marcio Diniz - *Mulata Dengosa*     
     From the multi-artist album **Nicola Conte presents Viagem Vol. 3** 

  #. Bossa Trio - *Tema em Do*     
     From the multi-artist album **Nicola Conte presents Viagem Vol. 3** 

  #. Clara Nunes - *A Deusa Dos Orixes*     
     From the multi-artist album **O Samba Brazil Classics 2** 

  #. Quantic Presenta Flowering Inferno - *Echate Palla*     
     From the album [Dog With A Rope](https://quanticmusic.bandcamp.com/album/dog-with-a-rope) 

  #. Ray Harris & The Fusion Experience - *In Your Eyes*     
     From the album **Ray Harris and the Fusion Experience** 

  #. Breakestra - *Dark Clouds Rain Soul*     
     From the album **Dusk Till' Dawn** 


# gmcd-2011-12-disk-1 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/09nHIrJMSjlt2M9akcm7dJ?si=e180379b10834f32) 
  Playlist created: 2011-12-22   
 
  #. Stevie Wonder - *Higher Ground*     
     From the album **Innervisions** 

  #. Al Jarreau - *Mas Que Nada*     
     From the album **Tenderness** 

  #. re:jazz - *Too Many Holes Feat. Ernesto*     
     From the album **Expansion** 

  #. Ned Doheny - *On And On*     
     From the album **Ned Doheny** 

  #. Baker Brothers - *Fly Like An Eagle featuring Matt Mcnaughton*     
     From the album **Avid Sounds** 

  #. The Q-Continuum - *Organ Kane*     
     From the album **The Quintessential Grooves Vol. 1** 

  #. Betty Wright - *Old Songs*     
     From the album **Betty Wright: The Movie** 

  #. Patricia Barber - *C'est Magnifique*     
     From the album **The Cole Porter Mix** 

  #. Anne Sofie von Otter Meets Elvis Costello - *Broken Bicycles-Junk*     
     From the album **For The Stars** 

  #. Nostalgia 77 - *Mockingbird*     
     From the album **The Sleepwalking Society** 

  #. Jeanne Cherhal - *Le Petit Voisin*     
     From the multi-artist album **Les P'tites Qui Piaffent** 

  #. Erykah Badu - *Appletree*     
     From the album **Baduizm** 

  #. Ray Charles and Betty Carter - *But on the Other Hand Baby*     
     From the album **Ray Charles and Betty Carter** 

  #. Lizz Wright - *This Is*     
     From the album **The Orchard** 

  #. Baker Brothers - *If You Want Me To Stay featuring Hamish Stuart*     
     From the album **Avid Sounds** 

  #. Bill Evans Trio - *I Fall In Love Too Easily*     
     From the album **Moonbeams** 

  #. High Five Quintet - *Ojos De Rojo*     
     From the album **Five For Fun** 

  #. Pink Martini - *Tempo Perdido*     
     From the album **Hey Eugene!** 


# gmcd-2010-12-disk-2 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/2q63Ef3vYSaCVQYjIbs8eq?si=faf3a06b30434ca0) 
  Playlist created: 2010-12-19   
 
  #. Daktaris - *Musicawa Silt*     
     From the album **Soul Explosion** 

  #. Curtis Mayfield - *Little Child Runnin' Wild*     
     From the album **Superfly** 

  #. The O'Jays - *Love Train*     
     From the album **The Essential O'Jays** 

  #. The Fabulous Three - *Answer Me Softly Pt1*     
     From the album **Truth & Soul presents The Best of The Fabulous Three** 

  #. Build An Ark - *Celebrate*     
     From the album **Love Part 1** 

  #. Beady Belle - *Diamonds In The Rough*     
     From the album [At Welding Bridge](https://www.beadybelle.com/discography) 

  #. Kinny - *Enough said featuring Quantic*     
     From the album [Idle Forest of Chit-Chat](https://www.amazon.com/Idle-Forest-Chit-Chat-Kinny/dp/B001J66K1M/) 

  #. Ocote Soul Sounds - *Vendendo Saude & Fe*     
     From the album [Coconut Rock](https://ocotesoulsounds.bandcamp.com/album/coconut-rock) 

  #. Micatone - *A part of me*     
     From the album [Ninesongs](https://micatone.bandcamp.com/album/ninesongs) 

  #. Quantic Presenta Flowering Inferno - *No Soy Del Valle*     
     From the album [Dog With A Rope](https://quanticmusic.bandcamp.com/album/dog-with-a-rope) 

  #. Mark Murphy - *My Foolish Heart*     
     From the album **Love Is What Stays** 

  #. Build An Ark - *Sunflowers in my Garden*     
     From the album **Love Part 1** 

  #. Bonobo - *We Could Forever*     
     From the album [Black Sands](https://bonobomusic.bandcamp.com/album/black-sands) 

  #. Jack Wilson - *Most Unsoulful Woman*     
     From the album **Something Personal** 

  #. Part Time Heroes - *Realise featuring Jono Mccleery*     
     From the album [Meanwhile](https://part-timeheroes.bandcamp.com/album/meanwhile) 

  #. Elvis Costello and Allen Toussaint - *The River In Reverse*     
     From the album **The River In Reverse** 

  #. Average White Band - *Got The Love*     
     From the album **AWB** 


# gmcd-2010-12-disk-1 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/3FhwYDCSseRztxRKBdzhBk?si=890e252ab3294e6d) 
  Playlist created: 2010-12-19   
 
  #. Mark Murphy - *Twisted*     
     From the album **Rah!** 

  #. [dunkelbunt] - *Watcha Clan, Balkan Qoulou [Dunkelbunt] Remix Feat. Cloud Tissa & MC Killo Killo*     
     From the album **Raindrops and Elephants** 

  #. Mario Biondi and the High Five Quintet - *I Can't Keep From Cryin' Sometimes*     
     From the album **A Handful of Soul** 

  #. Nino Moschella - *If You Believe You Will Be Strong*     
     From the album **The Fix** 

  #. Olive et moi - *L'ascenseur*     
     From the multi-artist album **Le Pop 5** 

  #. Plume - *L'éléphant Géant*     
     From the album **Même Pas Peur** 

  #. Donny Hathaway - *The Ghetto*     
     From the album **A Donny Hathaway Collection** 

  #. Ray Charles - *Drown In My Own Tears*     
     From the album **Rock & Roll** 

  #. Jeb Loy Nichols - *Sometime Somewhere Somebody*     
     From the album [Strange Faith And Practice](https://jebloynichols.bandcamp.com/album/strange-faith-and-practice) 

  #. Bei Bei & Shawn Lee - *The Tiger*     
     From the album **Into The Wind** 

  #. Bronx River Parkway - *Nora Se Va*     
     From the album **San Sebastian 152** 

  #. Allen Toussaint - *St James Infirmary*     
     From the album **The Bright Mississippi** 

  #. Ray Harris & The Fusion Experience - *Tokyo Blue*     
     From the album **Ray Harris and the Fusion Experience** 

  #. Ocote Soul Sounds - *Coconut Rock*     
     From the album [Coconut Rock](https://ocotesoulsounds.bandcamp.com/album/coconut-rock) 

  #. Lee Fields & The Expressions - *My World Is Empty Without You*     
     From the album **My World** 


# gmcd-2009-12-disk-2 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/2m7k319zngrGy3hbzumgqh?si=4c5863da273d4fc2) 
  Playlist created: 2009-12-19   
 
  #. Mark Murphy - *Stolen Moments Midnight Mood Rework*     
     From the multi-artist album **The Modern Sound of Nicola Conte Versions in Jazz-Dub** 

  #. Dalindèo - *Vintage Voyage*     
     From the album **Vintage Voyage EP** 

  #. Mayer Hawthorne - *Maybe So Maybe No*     
     From the multi-artist album **Brownswood Bubblers 4** 

  #. Part Time Heroes - *Realise featuring Jono Mccleery*     
     From the album [Meanwhile](https://part-timeheroes.bandcamp.com/album/meanwhile) 

  #. John Scofield - *Hit the Road Jack*     
     From the album [That's What I Say](http://www.johnscofield.com/discography/) 

  #. Fat Freddys Drop - *Boondigga*     
     From the album **Dr. Boondigga & The Big BW** 

  #. Slide Five - *By Chance*     
     From the album **Rhode Trip** 

  #. Spanky Wilson & The Quantic Soul Orchestra - *I'm Thankful Part 1*     
     From the album [I'm Thankful](https://tru-thoughts.bandcamp.com/album/im-thankful) 

  #. Quantic and his Combo Barbaro - *Un Canto A Mi Tierra*     
     From the album [Tradition In Transition](https://quanticmusic.bandcamp.com/album/tradition-in-transition) 

  #. Lee Fields - *Problems*     
     From the multi-artist album **Soul Fire The Majestic Collection** 

  #. Tom Waits - *Jockey Full of Bourbon*     
     From the album **Rain Dogs** 

  #. S-Tone Inc. - *Beira Do Mar*     
     From the multi-artist album **A History of Schema Il Giorno Della Mimosa** 

  #. Jose Mauro - *Obnoxious*     
     From the multi-artist album **Gilles Peterson Brazilika** 

  #. Seu Jorge - *Chega No Suingue*     
     From the album **Carolina** 

  #. José James - *blackeyedsusan*     
     From the album **The Dreamer** 

  #. Anamaria Bom - *Queimada*     
     From the multi-artist album **Nicola Conte Presents Viagem Vol. 2** 

  #. Ben Westbeech - *So Good Today feat. the Dapkings*     
     From the album **Welcome to the Remixes** 

  #. Fertile Ground - *Yellow Daisies*     
     From the multi-artist album **The Modern Sound of Nicola Conte Versions in Jazz-Dub** 


# gmcd-2009-12-disk-1 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/6zRAShDLXBLsKjzqu1gxNp?si=75b2884082af43de) 
  Playlist created: 2009-12-19   
 
  #. Mario Biondi - *Rio De Janeiro Blue*     
     From the multi-artist album **A History of Schema Coisa Mais Linda** 

  #. Raul Midon - *If You're Gonna Leave*     
     From the album **State of Mind** 

  #. The Rhythm Section - *Waiting For The Sun*     
     From the multi-artist album **The Best of Cookin' Ubiquity** 

  #. Quantic and his Combo Barbaro - *Linda Morena*     
     From the album [Tradition In Transition](https://quanticmusic.bandcamp.com/album/tradition-in-transition) 

  #. El Michels Affair - *Hung Up On My Baby*     
     From the album **Truth & Soul presents A Tribute to Isaac Hayes** 

  #. Daktaris - *Musicawa Silt*     
     From the album **Soul Explosion** 

  #. Friends From Rio 2 - *Os Escravos Do Jo featt. Celia Vaz*     
     From the multi-artist album **Gilles Peterson Brazilika** 

  #. Thunderball - *Elevated States*     
     From the multi-artist album **The Modern Sound of Nicola Conte Versions in Jazz-Dub** 

  #. John Scofield - *What'd I Say*     
     From the album [That's What I Say](http://www.johnscofield.com/discography/) 

  #. Quasimode - *Oneself - Likeness*     
     From the album **Oneself Likeness** 

  #. Tenorio Jr - *Consolação*     
     From the multi-artist album **Nicola Conte Presents Viagem Vol. 2** 

  #. Fragmentorchestra - *Carioca*     
     From the album [Fragmentorchestra](https://fragmentorchestra.bandcamp.com/album/fragmentorchestra) 

  #. Nino Moschella - *The Fix*     
     From the album **The Fix** 

  #. Jazzanova - *Let Me Show Ya feat. Paul Randolph*     
     From the album **Of All The Things** 

  #. Jesse Dee - *Around Here*     
     From the album **Bittersweet Batch** 

  #. Willie Nelson - *Blue Skies*     
     From the album **Stardust** 


# gmcd-2009-05-disk-2 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/2Ht4u7PLTUq2Vmgd2JeKzK?si=5dfaa42832fa4c85) 
  Playlist created: 2009-05-01   
 
  #. Willis - *Word Up!*     
     From the multi-artist album **Rewind! 4** 

  #. The Five Corners Quintet - *Kerouac Days in Montana*     
     From the album **Hot Corner** 

  #. Aaron Jerome - *Dancing Girl feat Mozez*     
     From the album **Time To Rearrange** 

  #. Jazzanova - *I Can See feat. Ben Westbeech*     
     From the album **Of All The Things** 

  #. Greyboy - *Mastered The Art featuring Elgin Park and Dave Pike Nicola*     
     From the album **15 Years of West Coast Cool** 

  #. Elvis Costello - *My All Time Doll*     
     From the album **Secret, Profane and Sugarcane** 

  #. Bïa - *La Nuit Des Masques*     
     From the album **La Memoire Du Vent** 

  #. Recloose - *Deeper Waters feat. Joe Dukie Eva Be Secret Lover Remix*     
     From the multi-artist album [Secret Love 5](https://sonarkollektiv.bandcamp.com/album/secret-love-5-compiled-by-jazzanova) 

  #. José James - *Park Bench People*     
     From the album **The Dreamer** 

  #. Nostalgia 77 - *Film Blues*     
     From the album **Nostalgia 77 Sessions feat Keith and Julie Tippett** 

  #. Shawn Lee - *Whatever Side You're On feat. Paul Butler*     
     From the album **Soul in the Hole** 

  #. Christian Prommer's Drumlesson - *Hear Us Now*     
     From the album [The Jazz Thing](https://thediningrooms.bandcamp.com/album/the-jazz-thing) 

  #. El Michels Affair - *Walk on By Burt Bacharach*     
     From the multi-artist album **Rewind! 5** 

  #. Raphael Saadiq - *Big Easy feat. The Infamous Young Spodie and the Rebirth Brass Band*     
     From the album **The Way I See It** 

  #. Kinny - *Petrified Dazed featuring TM Juke*     
     From the album [Idle Forest of Chit-Chat](https://www.amazon.com/Idle-Forest-Chit-Chat-Kinny/dp/B001J66K1M/) 

  #. Rosalia de Souza - *Carolina Carol Bela*     
     From the album [d'Improvviso](https://rosaliadesouza.bandcamp.com/album/dimprovviso) 

  #. The Menahan Street Band - *Birds*     
     From the album [Make The Road By Walking](https://menahanstreetband.bandcamp.com/album/make-the-road-by-walking) 


# gmcd-2009-05-disk-1 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/6y02LIcvHCHujXQ6sV9oGu?si=57fa524aac134ee9) 
  Playlist created: 2009-05-01   
 
  #. The Dining Rooms - *Ocean Diggin In The Latin Mix*     
     From the multi-artist album **Break 'n Bossa Chapter 8** 

  #. Shawn Lee - *Soul In The Hole*     
     From the album **Soul in the Hole** 

  #. Raphael Saadiq - *Never Give You Up feat. Stevie Wonder and CJ*     
     From the album **The Way I See It** 

  #. Nostalgia 77 - *You Don't Just Dream When You Sleep*     
     From the album **Nostalgia 77 Sessions feat Keith and Julie Tippett** 

  #. Mario Biondi and The High Five Quintet - *This Is What You Are*     
     From the multi-artist album **A History of Schema** 

  #. Outlines - *Listen To The Drums Clignancourt Edit*     
     From the album **Our Lives Are Too Short** 

  #. Aaron Jerome - *Late Night Mission The Bump feat Yungun*     
     From the album **Time To Rearrange** 

  #. Q-Tip f. Stevie Wonder - *What the Fuss? Shook Remix*     
     From the multi-artist album **J.Period Presents... The [Abstract] Best** 

  #. Build An Ark - *Healing Song*     
     From the album [Dawn](https://www.amazon.com/Dawn-Build-Ark/dp/B000W352IU) 

  #. Kinny - *Enough said featuring Quantic*     
     From the album [Idle Forest of Chit-Chat](https://www.amazon.com/Idle-Forest-Chit-Chat-Kinny/dp/B001J66K1M/) 

  #. Sound Directions - *Dice Game*     
     From the album **The Funky Side of Life** 

  #. Emily Jones - *Espontaneo*     
     From the album **Gerardo Frisina Introduces Emily Jones EP** 

  #. Clutchy Hopkins - *Love of a Woman*     
     From the album **Walking Backwards** 

  #. S-Tone Inc. - *Lua E Xango*     
     From the album [Moon in Libra](https://s-toneinc.bandcamp.com/album/moon-in-libra) 

  #. Rosalia de Souza - *Opiniao*     
     From the album [d'Improvviso](https://rosaliadesouza.bandcamp.com/album/dimprovviso) 

  #. The Five Corners Quintet - *Midnight In Trieste*     
     From the album **Hot Corner** 

  #. S-Tone Inc. - *Stormy*     
     From the album [Moon in Libra](https://s-toneinc.bandcamp.com/album/moon-in-libra) 


# gmcd-2008-12-disk-2 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/0CmA3Bg0Fh7o2zCBwvnW1e?si=638d5fd470dd40e5) 
  Playlist created: 2008-12-28   
 
  #. Waldeck - *Jerry Weintraub*     
     From the album **Ballroom Stories** 

  #. Elizabeth Shepherd - *Parkdale*     
     From the album **Parkdale** 

  #. Loopless - *Is The Phone Broke Or Something?*     
     From the album **Loopless** 

  #. Little Dragon - *After The Rain*     
     From the album **Little Dragon** 

  #. The RH Factor - *I'll Stay*     
     From the album **Hard Groove** 

  #. The Stance Brothers - *Dynamite*     
     From the album **Kind Soul** 

  #. Jazzanova - *Dial A Cliché feat. Paul Randolph*     
     From the album **Of All The Things** 

  #. Willie Nelson & Wynton Marsalis - *Ain't Nobody's Business*     
     From the album **Two Men With The Blues** 

  #. Beady Belle - *Tower Of Lament*     
     From the album [Belvedere](https://www.beadybelle.com/discography) 

  #. Christian Prommer's Drumlesson - *Claire*     
     From the album **Drumlesson Vol 1** 

  #. Two Banks of Four - *Dead Afternoon*     
     From the album **Junkyard Gods** 

  #. Yesterdays New Quintet - *Street Talkin' - Kamala Walker & The Soul Tribe*     
     From the album **Yesterdays Universe Prepare For A New Yesterday** 

  #. Spaceways Incorporated - *You And Your Folks, Me And My Folks*     
     From the album **Radiale** 

  #. Alice Russell - *Crazy*     
     From the album **Pot of Gold** 

  #. Waldeck - *Bei mir bist Du schön Dub*     
     From the album **Ballroom Stories** 

  #. Nostalgia 77 Octet - *Changes*     
     From the album **Sevens & Eights Recorded Live At The Jazz Café** 


# gmcd-2008-12-disk-1 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/2m7k319zngrGy3hbzumgqh?si=825da200c0574ae4) 
  Playlist created: 2008-12-28   
 
  #. Nomo - *New Song*     
     From the album **New Tones** 

  #. James Moody & His Bop Men w Art Blakey - *Tin Tin Deo*     
     From the multi-artist album **Blue Note Trip Lookin' Back and Movin' On** 

  #. Sound Directions - *Dice Game*     
     From the album **The Funky Side of Life** 

  #. Gerardo Frisina - *Gosto De Que E Bom*     
     From the multi-artist album **A History of Schema Tokyo's Dream** 

  #. Suba - *Felicidade feat. Cibelle*     
     From the album **Sao Paulo Confessions** 

  #. Randy Weston - *In Memory Of*     
     From the multi-artist album **The Kings of Jazz comp. by Gilles Peterson and Jazzanova** 

  #. Alice Russell - *All Alone*     
     From the album **Pot of Gold** 

  #. Baby Charles - *I Bet You Look Good On The Dancefloor*     
     From the album **Baby Charles** 

  #. Yaw - *Where Would You Be*     
     From the multi-artist album **Brownswood Bubblers 3** 

  #. Little Dragon - *Scribbled Paper*     
     From the multi-artist album **Neujazz** 

  #. Beady Belle - *Boiling Milk*     
     From the album [Belvedere](https://www.beadybelle.com/discography) 

  #. Elizabeth Shepherd - *Sicilienne*     
     From the album **Parkdale** 

  #. Yegelle Tezeta - *My Own Memory*     
     From the multi-artist album **Ethiopiques Vol. 4 Ethio Jazz & Musique Instrumentale** 

  #. Nostalgia 77 - *Stop to Make a Change*     
     From the album **Everything Under The Sun** 

  #. Build An Ark - *River Run*     
     From the album [Dawn](https://www.amazon.com/Dawn-Build-Ark/dp/B000W352IU) 

  #. Irene Kral - *Going To California*     
     From the multi-artist album **Gilles Peterson Digs America 2** 

  #. Dave Pike - *Regards from Freddie Horovitz*     
     From the multi-artist album [The Basic Principles Of Sound 2](https://www.discogs.com/Various-The-Basic-Principles-Of-Sound-Music-For-The-Modern-Listener-Volume-2/release/1208823) 

  #. Sonar Kollektiv Orchester - *Run / Fedimes Flight*     
     From the album [Guaranteed Niceness](https://sonarkollektiv.bandcamp.com/album/guaranteed-niceness) 


# gmcd-2007-12 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/5PJ5CVQEFHXUFtMQZGle5J?si=023eb96d549044ec) 
  Playlist created: 2007-12-28   
 
  #. Cinematic Orchestra - *Ode to the Big Sea*     
     From the album **Motion** 

  #. Freddie Cole - *Brother Where Are You*     
     From the multi-artist album **The Best of Jazzman Records** 

  #. The Quantic Soul Orchestra - *She Said What? Rap Descarga*     
     From the album [Tropidelico](https://www.amazon.com/Tropidelico-Quantic-Soul-Orchestra/dp/B000VQQW00) 

  #. Bellcrash - *White Jazz*     
     From the album [Suzume Park](https://www.discogs.com/Bellcrash-Suzume-Park/release/380495) 

  #. Ursula Rucker - *Circe Jazzanova Mix*     
     From the multi-artist album [Jazzanova-The Remixes 1997-2000](https://jazzanova.bandcamp.com/album/the-remixes-1997-2000) 

  #. Belleruche - *13:6:35*     
     From the album [Turntable Soul Music](https://belleruche.bandcamp.com/album/turntable-soul-music) 

  #. Dalminjo - *Goodbye feat Alexandra Hamnede*     
     From the album [Fjord Fusioneer](https://www.discogs.com/Dalminjo-Fjord-Fusioneer/release/253892) 

  #. Fat Freddys Drop - *Ernie*     
     From the album **Based On A True Story** 

  #. Micatone - *Sit Beside Me*     
     From the album [Is You Is](https://micatone.bandcamp.com/album/is-you-is) 

  #. Nostalgia 77 - *Changes*     
     From the album **The Garden** 

  #. The Quantic Soul Orchestra - *Lead Us To The End Soul*     
     From the album [Tropidelico](https://www.amazon.com/Tropidelico-Quantic-Soul-Orchestra/dp/B000VQQW00) 

  #. Moreno Veloso + 2 - *Deusa Do Amor*     
     From the multi-artist album **Putumayo Presents Samba Bossa Nova** 

  #. The Dining Rooms - *Ink*     
     From the album **Ink** 

  #. Cinematic Orchestra - *Child Song*     
     From the album **Ma Fleur** 

  #. TM Juke - *Come Away feat Sophie Faricy*     
     From the album **Forward** 

  #. Thief - *Does It Make Any Sense*     
     From the multi-artist album [Secret Love 3](https://sonarkollektiv.bandcamp.com/album/secret-love-3-compiled-by-jazzanova-resoul) 

  #. Jazzanova - *Hanazono*     
     From the album **In Between** 

  #. Jehro - *Everything*     
     From the album **Jehro** 

  #. Tuung - *Jenny Again*     
     From the multi-artist album [Secret Love 3](https://sonarkollektiv.bandcamp.com/album/secret-love-3-compiled-by-jazzanova-resoul) 

  #. 1Luv - *If U Can Be Strong*     
     From the album **Neophilia** 

  #. Radio Citizen - *Everything feat. Bajka*     
     From the album [Berlin Serengeti](https://radiocitizenubiq.bandcamp.com/album/berlin-serengeti) 

  #. Kuusumun Profeetta - *Kovin Lentäen Kotiin Kaipaan*     
     From the multi-artist album **Jazzflora** 

  #. Radio Citizen - *El Cielo feat. Bajka*     
     From the album [Berlin Serengeti](https://radiocitizenubiq.bandcamp.com/album/berlin-serengeti) 

  #. The Budos Band - *King Cobra*     
     From the album **The Budos Band II** 

  #. Belleruche - *Northern Girls*     
     From the album [Turntable Soul Music](https://belleruche.bandcamp.com/album/turntable-soul-music) 

  #. Quantic - *Don't Joke With a Hungry Man*     
     From the album [Mishaps Happening](https://quanticmusic.bandcamp.com/album/mishaps-happening) 

  #. Cinematic Orchestra - *Evolution feat. Fontella Bass*     
     From the album **Every Day** 

  #. Nostalgia 77 - *Dreamers Dance*     
     From the album **Everything Under The Sun** 

  #. Jehro - *Master Blaster Demo Version*     
     From the album **Jehro** 


# gmcd-2007-08 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/6mvxb5xc2lNbvOvURHhQqR?si=809350f758974884) 
  Playlist created: 2007-08-01   
 
  #. Alice Russell - *All else can wait*     
     From the album **My Favourite Letters** 

  #. Alice Russell - *Munkaroo*     
     From the album **My Favourite Letters** 

  #. The Budos Band - *The Volcano Song*     
     From the album **The Budos Band** 

  #. TM Juke - *Damn feat Kinny*     
     From the album **Forward** 

  #. Kinny and Horne - *Forgetting To Remember*     
     From the album [Forgetting To Remember](https://www.amazon.com/Forgetting-Remember-Kinny-Horne/dp/B0008G2FZO/) 

  #. CéU - *Malemolencia*     
     From the album **CéU** 

  #. Pathless - *Forecast*     
     From the multi-artist album **Paz E Futebol Compiled By Jazzanova** 

  #. Jazzanova - *L.O.V.E. and You & I*     
     From the album **In Between** 

  #. Lorez Alexandria - *Morning*     
     From the multi-artist album [The Basic Principles Of Sound 2](https://www.discogs.com/Various-The-Basic-Principles-Of-Sound-Music-For-The-Modern-Listener-Volume-2/release/1208823) 

  #. The Dining Rooms - *Forever's Not*     
     From the album **Experiments In Ambient Soul** 

  #. Alice Russell - *To know this*     
     From the album **My Favourite Letters** 

  #. Alice Russell - *Somebodys gonna love you feat. Quantic*     
     From the album **Under the Munka Moon** 

  #. Micatone - *Mars*     
     From the album [Nomad Songs](https://micatone.bandcamp.com/album/nomad-songs) 

  #. Micatone - *Shake It Baby*     
     From the album [Is You Is](https://micatone.bandcamp.com/album/is-you-is) 

  #. Cinematic Orchestra - *All that you give*     
     From the album **Every Day** 


# gmcd-2006-12 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/6PN8Wqqrij4nc8ra6JxCWT?si=facf04ecbcf449e8) 
  Playlist created: 2006-12-26   
 
  #. Alice Russell - *Somebodys gonna love you feat. Quantic*     
     From the album **Under the Munka Moon** 

  #. The Dining Rooms - *Diamonds & Comforts*     
     From the album **Experiments In Ambient Soul** 

  #. Bonobo - *Days To Come*     
     From the album [Days To Come](https://bonobomusic.bandcamp.com/album/days-to-come) 

  #. Mr Scruff - *Get A Move On*     
     From the multi-artist album **Saint Germain des Pres Cafe vol. 06** 

  #. A Bossa Elétrica - *Skindo-le-le*     
     From the album **Eletrificação** 

  #. CéU - *Lenda*     
     From the album **CéU** 

  #. Alice Russell - *Munkaroo*     
     From the album **My Favourite Letters** 

  #. Camille - *La Jeune Fille Aux Cheveux Blancs*     
     From the album **Le Fil** 

  #. Beady Belle - *Lose   Win*     
     From the album [Home](https://www.beadybelle.com/discography) 

  #. Bonobo - *Sugar Rhyme*     
     From the album [Animal Magic](https://bonobomusic.bandcamp.com/music) 

  #. CéU - *Concrete Jungle*     
     From the album **CéU** 

  #. A Bossa Elétrica - *Tudo Está Previsto*     
     From the album **Eletrificação** 

  #. Kinny and Horne - *Dignity*     
     From the album [Forgetting To Remember](https://www.amazon.com/Forgetting-Remember-Kinny-Horne/dp/B0008G2FZO/) 

  #. Beady Belle - *Goldilocks*     
     From the album [Closer](https://www.beadybelle.com/discography) 

  #. Bonobo - *Pick Up*     
     From the multi-artist album **It Came From The Sea Solid Steel Presents Bonobo** 

  #. Thievery Corporation - *Lebanese Blonde*     
     From the album [The Mirror Conspiracy](https://www.amazon.com/Mirror-Conspiracy-Thievery-Corporation/dp/B000S9FPX0/) 

  #. Nicola Conte - *Fuoco Fatuo Performed By Koop*     
     From the multi-artist album **Saint Germain des Pres Cafe vol. 04** 

  #. Eighty Mile Beach - *Inclement Weather*     
     From the album **Inclement Weather** 

  #. Patricia Barber - *Clues*     
     From the album **Verse** 

  #. Visioneers - *Replay Ft. Voice*     
     From the album **Dirty Old Hip Hop** 

  #. Nuspirit Helsinki - *Trying*     
     From the album **Nuspirit Helsinki** 

  #. The Quantic Soul Orchestra - *Feeling Good Feat Alice Russell*     
     From the album **Pushin' On** 

  #. Quantic - *Transatlantic*     
     From the album [Apricot Morning](https://quanticmusic.bandcamp.com/album/apricot-morning) 

  #. Stereolab - *Margerine Melodie*     
     From the album **Margerine Eclipse** 

  #. Patricia Barber - *Dansons La Gigue*     
     From the album **Verse** 

  #. Alice Russell - *Mirror mirror on the wolf `tell the story right`*     
     From the album **My Favourite Letters** 

  #. The Future Sound Of London - *Papua New Guinea*     
     From the album **Accelerator** 


# gmcd-2006-01 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/2BVgtcj9rYs6z4NLuv5x2Y?si=145d6bbabaef46eb) 
  Playlist created: 2006-01-02   
 
  #. Desmond Williams - *Um Favor*     
     From the album **Delights Of The Garden** 

  #. David Bowie - *I've Been Waiting For You*     
     From the album **Heathen** 

  #. The Five Corners Quintet - *Before We Say Goodbye feat Mark Murphy*     
     From the album **Chasin' The Jazz Gone By** 

  #. South San Gabriel - *New Brookland*     
     From the album **Welcome, Convalescence** 

  #. Bebel Gilberto - *Simplesmente Tom Middleton Remix*     
     From the album **Bebel Gilberto Remixed** 

  #. Sarah McLachlan - *World on Fire*     
     From the album **Bloom Remix Album** 

  #. The Five Corners Quintet - *Case Study feat Okou*     
     From the album **Chasin' The Jazz Gone By** 

  #. David Bowie - *Quicksand*     
     From the album **Hunky Dory** 

  #. Tom Waits - *Starving in the Belly of a Whale*     
     From the album **Blood Money** 

  #. Bonobo - *Sleepy Seven*     
     From the album [Animal Magic](https://bonobomusic.bandcamp.com/music) 

  #. David Bowie - *Amsterdam*     
     From the album **Bowie at the Beeb** 

  #. Nuspirit Helsinki - *Honest*     
     From the album **Nuspirit Helsinki** 

  #. Flunk - *Honey's In Love*     
     From the album **For Sleepyheads Only** 

  #. The Postal Service - *This Place Is a Prison*     
     From the album **Give Up** 

  #. Death Cab for Cutie - *Title and Registration*     
     From the album **Transatlanticism** 

  #. Bonobo - *Pick Up Four Tet Remix*     
     From the album [Live Sessions](https://bonobomusic.bandcamp.com/music/) 


# gmcd-2005-07 
  [Spotify playlist URL     
](https://open.spotify.com/playlist/4TW4YyOYJIF4gFpPlOTx06?si=0f985f5b8ce94049) 
  Playlist created: 2005-07-01   
 
  #. Stereolab - *Doubt*     
     From the album **ABC Music Radio 1 Sessions** 

  #. King Kooba - *Barefoot*     
     From the album **Indian Summer** 

  #. Rickie Lee Jones - *Dat Dere*     
     From the album **Pop Pop** 

  #. John Hammond - *Get Behind the Mule*     
     From the album **Wicked Grin** 

  #. Ivy - *Disappointed*     
     From the album **Long Distance** 

  #. Flunk - *Blue Monday*     
     From the album **For Sleepyheads Only** 

  #. The Dining Rooms - *False Start*     
     From the album **Numero Deux** 

  #. Cat Power - *Shaking Paper*     
     From the album **You Are Free** 

  #. John Prine - *In Spite of Ourselves feat. Iris DeMent*     
     From the album **In Spite Of Ourselves** 

  #. Happy Mondays - *Loose Fit*     
     From the multi-artist album **24 Hour Party People** 

  #. Medeski Martin and Wood - *Macha*     
     From the album **Best Of 1991-1996** 

  #. St. Thomas - *Goodbye Emily Lang*     
     From the album **I'm Coming Home** 

  #. A Guy Called Gerald - *Voodoo Ray*     
     From the multi-artist album **24 Hour Party People** 

  #. Tom Waits - *How's It Gonna End*     
     From the album **Real Gone** 

  #. Cat Power - *Speak For Me*     
     From the album **You Are Free** 

  #. John Hammond - *I Know I've Been Changed*     
     From the album **Wicked Grin** 

































