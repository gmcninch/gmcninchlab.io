---
author: George McNinch
title: Bio of George McNinch
---


+ The origin of [Stencilv], explained
+ I study and teach
  [mathematics](http://gmcninch-tufts.github.io); I'm a professor
  in the [math department](http://math.tufts.edu) at [Tufts
  University](http://www.tufts.edu).
+ I enjoy playing the saxophone.
+ I like to listen to music -- see [Last.fm <i class="fab
  fa-lastfm-square"></i>](https://www.last.fm/user/stencilv).


+ Once upon a time -- and perhaps still? -- I was a pretty avid fan of
  ``sci-fi`` novels: [Goodreads <i class="fab
  fa-goodreads"></i>](https://www.goodreads.com/user/show/71900088-george-mcninch)
  
+ I am an enthusiastic bicycle rider (both for commuting and recreation).

+ And I have a hobbyist-interest in [`GNU/Linux`] and [`Emacs`] and
  `functional programming` -- including [`Haskell`], [`Idris`], ...

[Stencilv]: /posts/2019-05--stencilv.html
[`GNU/Linux`]: http://www.gnu.org
[`Ocaml`]: http://ocaml.org
[`Haskell`]: https://www.haskell.org/
[`Purescript`]: https://www.purescript.org/
[`Emacs`]: https://www.gnu.org/software/emacs/
[`Idris`]: https://www.idris-lang.org/index.html

::::::{.image-container-2col}
[![ref0]](/assets/images/2018-11-21 13.07.30.jpg)

[![ref1]](/assets/images/2015--saxophone.jpg)
::::::

[ref0]: /assets/images/2018-11-21 13.07.30.jpg "" {.image}


[ref1]: /assets/images/2015--saxophone.jpg "" {.image}

