
(use-modules (ice-9 ftw)
	     (util uuid-gen))

(map
 (lambda (f) (copy-file f (string-append (uuid-gen) ".jpg")))
 (scandir "."
	  (lambda (x) (string-match "jpg$" x))))

