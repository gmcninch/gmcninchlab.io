---
title: Television 2022
date: 2022-12-27
tags: television, media
summary: 
---
**This post is part of the [media](2022-12--media.html) series**

Roughly 10 years ago, my wife and I began regularly watching
serialized shows on streaming services -- Netflix, Amazon Prime, etc.
It has now become a habit -- typically, we are watching some series,
and we watch an episode of that series each evening after dinner.

# During 2022, my wife and I watched:

- Better Call Saul (all seasons)

::: {.image-container-1col}
[![img1]](/assets/images/better-call-saul-image.jpg)
:::

[img1]: /assets/images/better-call-saul-image.jpg {.med-image}


- White Lotus (season 2)

::: {.image-container-1col}
[![img2]](/assets/images/official-poster-for-the-white-lotus-season-2.jpg)
:::

[img2]: /assets/images/official-poster-for-the-white-lotus-season-2.jpg {.med-image}


- Succession (season 3)

::: {.image-container-1col}
[![img3]](/assets/images/succession-season-3.png)
:::

[img3]: /assets/images/succession-season-3.png {.med-image}


- Yellowjackets (season 1)


::: {.image-container-1col}
[![img4]](/assets/images/yelowjackets.jpg)
:::

[img4]: /assets/images/yellowjackets.jpg {.med-image}


- The Queen's Gambit


::: {.image-container-1col}
[![img5]](/assets/images/the-queens-gambit-poster-scaled.jpg)
:::

[img5]: /assets/images/the-queens-gambit-poster-scaled.jpg {.med-image}



- The Sandman

::: {.image-container-1col}
[![img6]](/assets/images/the-sandman.jpg)
:::

[img6]: /assets/images/the-sandman.jpg {.med-image}



- Ozark (seasons 2-4)


::: {.image-container-1col}
[![img7]](/assets/images/ozark.jpg)
:::

[img7]: /assets/images/ozark.jpg {.med-image}




- Stranger Things (season 4)


::: {.image-container-1col}
[![img8]](/assets/images/stranger-things-4.jpg)
:::

[img8]: /assets/images/stranger-things-4.jpg {.med-image}


- Russian Doll (season 2)


::: {.image-container-1col}
[![img9]](/assets/images/russian-doll-season-2.jpg)
:::

[img9]: /assets/images/russian-doll-season-2.jpg {.med-image}


- The Expanse (season 6)


::: {.image-container-1col}
[![img0]](/assets/images/the-expanse-season-6.jpg)
:::

[img0]: /assets/images/the-expanse-season-6.jpg {.med-image}



# By myself, I watched

- Bojack Horseman (all seasons)


::: {.image-container-1col}
[![img10]](/assets/images/bojack.jpg)
:::

[img10]: /assets/images/bojack.jpg {.med-image}



- Peaky Blinders (all seasons)



::: {.image-container-1col}
[![img11]](/assets/images/peaky-blinders-season-5.jpg)
:::

[img11]: /assets/images/peaky-blinders-season-5.jpg {.med-image}


