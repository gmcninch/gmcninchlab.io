
On the NYR Daily this week

David Graeber (1961–2020), the radical anthropologist and author known
for his work on debt and “bullshit jobs,” movement politics and direct
action, died on September 2 at fifty-nine. His friend Astra Taylor,
documentary filmmaker, writer, and political organizer, orchestrated a
collection of tributes to Graeber by friends, colleagues, and
comrades, which we published on the Daily
[https://www.nybooks.com/daily/2020/09/05/david-graeber-1961-2020/]
over the course of the last week.

Graeber, as many have said, was a rare activist academic, an anarchist
as invested in organizing as in scholarship. The child of
working-class parents in New York, he showed an early interest in Maya
hieroglyphics and went on to study anthropology. He participated in
anti-globalization protests in the 2000s, against the IMF and WTO, and
in 2011 he helped to start Occupy Wall Street. After being
controversially denied promotion by Yale University, Graeber taught at
Goldsmiths College and at the London School of Economics. At the time
of his death, he had just finished a history of social inequality
spanning the last four millennia, co-written with the archaeologist
David Wengrow, who also wrote a remembrance for the Daily.

Last December, Graeber first contributed to the Review with “ Against
Economics
[https://www.nybooks.com/articles/2019/12/05/against-economics/],” in
which he argued that the basic assumptions on which the discipline of
economics is understood and taught are false, “designed to solve
another century’s problems,” with characteristic zest:

Mainstream economists nowadays might not be particularly good at
predicting financial crashes, facilitating general prosperity, or
coming up with models for preventing climate change, but when it comes
to establishing themselves in positions of intellectual authority,
unaffected by such failings, their success is unparalleled. One would
have to look at the history of religions to find anything like it.

This understanding of economics as a social science, not a law of
physics, and one subject to the manipulations of power, was central to
Graeber’s work, exemplified in his 2011, best-selling Debt: The First
5,000 Years, which exposed the historical contingency—and false
morality—of debt. Taylor told me in an email: “David’s book showed the
mutability of debt, and reminded us that debt is a promise corrupted
by money and violence. His intervention works on multiple levels. On
the one hand, it challenges the conventional economic narrative about
the interplay between credit and coinage. It reminds people of the
long history of debtors’ revolts. And it poses a deep philosophical
and moral challenge: Who owes what to whom? And what other promises
could we make to one another?”

Aside from his intellectual influence, it’s striking how many of the
remembrances have emphasized Graeber’s generosity and, as Taylor
writes, “how profoundly he embodied his egalitarian principles.” I
asked her to elaborate. “I’m not sure why this is so rare, but the
fact is many people profess radical or egalitarian ideals that they
don’t practice,” she said. “He always said anarchism isn’t an
identity, it’s a way of being, something you do—and he was true to
that formulation.”

Taylor first met Graeber in 2009, and they became fast friends. A
couple of years later, he asked her to attend the planning meetings of
what would become Occupy Wall Street. Taylor skipped those but did go
to the first assembly. With others, the two went on to form Strike
Debt, which instituted a “Rolling Jubilee,” an initiative that bought
up people’s medical debt and forgave it, demonstrating how debt is
turned into a commodity traded for profit, at the expense of
criminalized debtors. As Luke Herrine, a co-founder with Taylor of
another such project that came out of this organizing—the Debt
Collective, a union for debtors—writes in his remembrance, “I have
seen firsthand many times that if you can tell a debtor a story that
re-contextualizes their debt and allows them to reconsider creditor
morality, you turn immobilizing pain into politicizing anger.”

The post-Occupy anti-debt movement would go on to influence the policy
agendas of Elizabeth Warren and Bernie Sanders—and Graeber’s work
gained ever-wider currency. “Even as David became more renowned, he
stayed David,” Taylor said. “What would have surprised me would have
been to see David sell out.”

On the other side of the Atlantic, Graeber was a supporter of the UK’s
anti-tuition fee protests in 2010 and Jeremy Corbyn’s
[https://www.nybooks.com/daily/2020/01/13/the-center-blows-itself-up-care-and-spite-in-the-brexit-election/]
rise in the Labour Party. I asked Taylor how backing such a
traditional socialist squared with Graeber’s commitment to anarchism
and direct action, or, as he often said, “acting as if one is already
free.”

“David was an unabashed anarchist, but not a dogmatic one,” Taylor
told me. He cheered Sanders in the US and Corbyn in the UK, “even if
his vision of democracy and activist engagement exceeded and was
ultimately very critical of electoral politics.

“It’s very important, given David’s involvement in the Labour Party,
that we not mistake him for a social democrat,” Taylor
continued. “Let’s not soften his edges—something that happens to so
many radicals in their afterlives. But he also wasn’t sectarian, he
wanted people’s lives to be better and to see conditions out of which
an even more radical politics might emerge—and he wanted to prevent
fascists and racists from gaining more power. Of course, he also
despised liberal centrists, whose soulless triangulations are in many
ways responsible for Trumpism’s rise.”

Taylor added that his recent commitments also included the democratic
Kurdish liberation movement, and indeed he traveled to Rojava—even
smuggling drones into the region, as one tribute notes. Taylor and
Graeber shared a critical eye toward a certain nostalgia for American
democracy pre-2016, as a weak antidote to Trump’s MAGA era. In a
conversation following the publication of Taylor’s book Democracy May
Not Exist, But We’ll Miss It When It’s Gone
[https://us.macmillan.com/books/9781250179845] (2019) and release of
her film What Is Democracy? [https://www.whatisdemocracy.info/]
(2018), which explore how a fully inclusive and egalitarian democracy
has never existed and ask what self-government would truly mean, she
and Graeber discussed
[https://www.lrb.co.uk/podcasts-and-videos/podcasts/at-the-bookshop/astra-taylor-and-david-graeber-democracy-may-not-exist-but]
these fundamental questions.

Graeber’s commitment to democracy without a state—the idea that it’s
possible to have a society based on principles of self-organization,
voluntary association, and mutual aid—can be traced back to his work
as an anthropologist. “What’s most striking to me,” Taylor told me,
“is how he used the lens of anthropology to reflect on American
society, in his slightly estranged and bemused and ever-curious way.”
Graeber’s first book, and his best by his own account
[https://www.pmpress.org/blog/2020/09/03/in-loving-memory-david-graeber/],
Lost People: Magic and the Legacy of Slavery in Madagascar (2007), was
based largely on his earlier fieldwork. Among other things, Graeber
recognized
[https://africasacountry.com/2020/09/david-graeber-africanist]in the
Madagascar highlands possible alternatives to the socio-political
realities he’d known.

In that spirit, Graeber’s partner, Nika Dubrovsky, is organizing a
fundraiser
[https://www.patreon.com/user?u=32177115#no_universal_links] to turn
his childhood home into a memorial foundation and meeting place for
intellectuals, activists, and artists to imagine new futures
together. As he wrote: “The ultimate, hidden truth of the world is
that it is something that we make, and could just as easily make
differently.”

Dear David, 2020 (Molly Crabapple— see full image at NYR Daily
[https://www.nybooks.com/daily/2020/09/05/david-graeber-1961-2020/])
