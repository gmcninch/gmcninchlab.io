---
title: Bike to Work! (poster)
date: 2020-12-10
tags: cycling, web-artifact
summary: 
---

I came across this poster while poking around in "old stuff" on my
computer this morning. I remember that I kept a printed copy of this
image on my office door(s) for a number of years.

:::::::{.image-container-1col}
<!-- [![ref0](/assets/images/Bike-to-work.pdf) -->
[![ref0]](/assets/images/Bike-to-work.pdf)

[ref0]: /assets/images/Bike-to-work.png "poster" {.image}
:::::::

I believe that I first found it some time in the 90s in the form of an
old *postscript* file.

The postscript file begins with the following comment (I've elided
``<..>`` identifying info, though I guess it was a public document and
anyhow is now surely ancient history). It recalls the '90s internet
for me!

---------

```
%!PS
%It took 2 years of planning, and now the Internet Bike To Work T-shirt is
%a reality!  The PostScript artwork is attached below in the spirit of
%"sharewear"; you can use it as a poster for bike to work events, or as
%camera-ready copy for imprinting your own T-shirts, jerseys, etc.
%
%The main body is the three-arrowed recycling symbol with the words "Live to
%Bike to Work to..." along the arrows.  Within the recycling symbol is a
%picture of the earth.  Around the symbol is circular text "THINK GLOBALLY"
%and "CYCLE LOCALLY".  On top of everything is "BIKE TO WORK!"
%
%If you'd like to order a T-shirt (pre-shrunk 100% cotton) with the above
%artwork, I have limited quantities of the following:
%Short sleeve Oneita Power-T: aqua on yellow, L and XL, $11 US.
%Long sleeve Hanes Beefy-T: yellow on red (L and XL) or teal (XL), $14 US.
%I plan on shipping Parcel Post or UPS.  Quantity discounts and non-US shipping
%negotiable.  Send check payable to me at:
%	<...>
%	Room <...>
%	AT&T Bell Laboratories
%	2000 N. Naperville Rd.
%	PO BOX 3033
%	Naperville, IL 60566-7033
%
%Include your name, address, size, and color if long sleeve.  Also send me
%email so we can communicate in case of questions.  My phone: <...>.
%
%Credits:
%Recycling symbol:		<...>@mswe.ms.philips.nl
%Earth picture:			<...>@pinoko.berkeley.edu (<...>)
%Circle text help:		<...>@ctc.att.com (<...>)
%Live-to-Bike-to-Work slogan:	<...>@maine.maine.edu (<...>)
%Editor:			<...>@ctc.att.com (<...>)
```
---------

Sadly, I never had a T-shirt with this logo (I think the postscript
file was already old by the time I came upon it which I'd estimate was
in '97 or '98. Probably I discovered it in one of the
``rec.bicycling`` newsgroups?).
