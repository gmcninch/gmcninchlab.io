---
title   : What is up with the “stencilv” moniker?
date    : 2019-05-31
tags    : pynchon, stencilv, kitties
summary : |
  Who am I, anyway?
---

Sometime as the late 1990s and early 2000’s rolled past, I observed
that “everyone else has an online nickname or avatar” and it seemed
that I ought to have one, too. Those were the days before the advent
of modern-style social media. I was reading [books by
Lessig](http://www.lessig.org/books/); I was a
[GNU/Linux](https://www.gnu.org) fan/advocate. I generally had a bit
of a utopian view of “being online”.  Honestly, though, I wasn’t
really much of a *participant* -- mostly I thought the *idea* of an
online social space was cool, probably because of science-fiction
tropes that I grew up reading.

Years earlier, when my wife and I married in the early 1990s, we
adopted two cats. We named one of them *Paola* after a character in
[Thomas Pynchon’s novel V.](https://en.wikipedia.org/wiki/V.), a book
which had a big impact on me a couple years earlier while I was an
undergraduate University student.


:::::::{.image-container-1col}
[![ref1]](/assets/images/Pynchon-V.jpg)
:::::::
[ref1]: /assets/images/Pynchon-V.jpg "Pynchon: V cover image" {.image}

Our cat Paola passed away in 2009; here she is in a 1999 photo:

:::::::{.image-container-1col}
[![ref2]](/assets/images/1999--Paola-smaller.jpg)
:::::::

[ref2]: /assets/images/1999--Paola-smaller.jpg "Paola, 1999" {.image}

It seemed that the idea of using names from this novel was a pattern to
leverage! *Stencil* was another character in the same Pynchon
novel. Since presumably a desirable property of an online name is
uniqueness, the tension between this goal and the notion of a stencil
amused me. Despite this tension, the composite name *stencilv* seemed
likely to be unused, and -- to my ear, at least -- *stencilv* made a
reasonable sounding online name. *Voilà!*



But: having conjured the nickname, I didn’t immediately find a good
use for it; in the end, the failure of an account name to suggest my
actual identity mostly seemed annoying -- especially for something
like an email account. Some years later, and on a whim, I used the
*stencilv* nickname for my [last.fm
account](http://www.last.fm/user/stencilv) -- my previous objection
seemed less important for an account whose main task was keeping
[track of music I’d
scrobbled](https://en.wikipedia.org/wiki/Last.fm). (In fact, Last.fm
tells me I’ve been scrobbling as stencilv since August 2005).  The
nickname’s usage in *last.fm* created some momentum, so I used it
again a few years later for my [twitter
handle](https://twitter.com/stencilv). But my usage of the nickname
through twitter hasn’t been especially prominent; for several years, I
ignored the account. I now use twitter as a news-aggregator. (Well,
strictly speaking, I use [nuzzel](https://nuzzel.com/) as a news
aggregator, by pointing it at my twitter feed).  I interact with media
using links from twitter a fair amount, and I “like” and occasionally
“re-tweet” stuff, but I wouldn’t say that I use the platform
*socially*.

Now finally I’m using the nickname for this website/blog. With
luck, I’ll continue writing here, so maybe the name will last!?


:::::::{.image-container-1col}
[![ref3]](/assets/images/post-horn.png)
:::::::

[ref3]: /assets/images/post-horn.png "Pynchon post-horn" {.image}


      
