---
title: Not Chaucer...
date: 2021-06-22
tags: math, fortunes
summary: 
---

Speaking of the unix program called
[fortune](https://en.wikipedia.org/wiki/Fortune_%28Unix%29), years ago
it showed me something that made me laugh, and continues to make me
laugh every time I see it again. I'm not sure quite why it is so
hilarious to me -- e.g. my wife didn't seem to find it very funny. In
fact, I'm not sure I've met anyone else who thinks it is as funny as I
do. But I'm going to post it here, as an identifying mark.

> Meanehwael, baccat meaddehaele, monstaer lurccen;  
> Fulle few too many drincce, hie luccen for fyht.  
> [D]en Hreorfneorht[d]hwr, son of Hrwaerow[p]heororthwl,  
> AEsccen aewful jeork to steop outsyd.  
> [P]hud!  Bashe!  Crasch!  Beoom!  [D]e bigge gye  
> Eallum his bon brak, byt his nose offe;  
> Wicced Godsylla waeld on his asse.  
> Monstaer moppe fleor wy[p] eallum men in haelle.  
> Beowulf in bacceroome fonecall bemaccen waes;  
> Hearen sond of ruccus saed, "Hwaet [d]e helle?"  
> Graben sheold strang ond swich-blaed scharp  
> Sond feorth to fyht [d]e grimlic foe.  
> "Me," Godsylla saed, "mac [d]e minsemete."  
> Heoro cwyc geten heold wi[p] faemed half-nelson  
> Ond flyng him lic frisbe bac to fen.  
> Beowulf belly up to meaddehaele bar,  
> Saed, "Ne foe beaten mie faersom cung-fu."  
> Eorderen cocca-colha yce-coeld, [d]e reol [p]yng   
>  
>>    -- Not Chaucer, for certain  

