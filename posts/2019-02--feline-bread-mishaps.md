---
title: A feline bread mishap
date: 2019-02-02
tags: bread, hobbies, food, kitties
summary:  They weren't supposed to step *there*!!
---

One of the two kitties pictured below (that is Jingle on the left,
Holly on the right) apparently jumped onto the kitchen counter and
stepped on my rising bread loaves. Well, at least the loaf had a towel
over it!

Not to worry, though -- the bread bounced back in the oven!

::::::::{.image-container-1col}
[![ref0]](/assets/images/2016--kitties.jpg)
::::::::

::::::::{.image-container-2col}
[![ref1]](/assets/images/2019-02--Sourdough-kitty--1-annotated.jpg)

[![ref2]](/assets/images/2019-02--Sourdough-kitty--2-annotated.jpg)
::::::::

[ref0]: /assets/images/2016--kitties.jpg "" {.image}
[ref1]: /assets/images/2019-02--Sourdough-kitty--1-annotated.jpg "" {.image}
[ref2]: /assets/images/2019-02--Sourdough-kitty--2-annotated.jpg "" {.image}


