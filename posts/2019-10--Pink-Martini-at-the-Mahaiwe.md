---
title: Pink Martini performed at the Mahaiwe Theater
author: George McNinch
date:  2019-10-18
tags: music
summary: Pink Martini performed at the Mahaiwe Theater in Great Barrington, Massachusetts
---
	  
We saw [Pink Martini] perform in the [Mahaiwe Theater] in [Great
Barrington, MA] (in the Berkshires). It was a nice show!

Along with the current core of the band, there were vocal performances
by [Edna Vazquez] and audience-surfing/vocals by [Meow Meow].

[Great Barrington, MA]: https://en.wikipedia.org/wiki/Great_Barrington,_Massachusetts
[Pink Martini]: http://www.pinkmartini.com
[Mahaiwe Theater]: https://www.mahaiwe.org/
[Edna Vazquez]: http://www.ednavazquez.com/
[Meow Meow]: https://meowmeowrevolution.com/

:::::::{.image-container-2col}
[![ref1]](/assets/images/2019-10--Pink Martini tickets.jpg)

[![ref2]](/assets/images/Pink-Martini--Hey-Eugene.jpg)
:::::::

[ref1]: /assets/images/2019-10--Pink Martini tickets.jpg "tickets" {.image}
[ref2]: /assets/images/Pink-Martini--Hey-Eugene.jpg "Hey Eugene" {.image}

