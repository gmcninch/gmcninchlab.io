---
title: Some performances
date: 2022-01-30
tags: jazz
summary: Pics
---

So I enjoy playing my saxophone -- mostly with an ensemble at
[Morningside Music Studio](http://www.morningsidemusicstudio.com/).
We had a performance in January at
[Sanctuary](https://sanctuarymaynard.com/) in Maynard, MA.  And some
of the the same group played at [Porchfest in
Somerville](http://somervilleartscouncil.org/porchFest)

I didn't end up with *group photos* but here are some snaps of me...

:::: {.image-container-2col}
[![ref]](/assets/images/2022-02--George-TheSanctuary.jpg)

[![ref1]](/assets/images/2021-09--Porchfest.jpg)
::::

[ref]: /assets/images/2022-02--George-TheSanctuary.jpg "at the sanctuary" {.image}


[ref1]: /assets/images/2021-09--Porchfest.jpg "Somerville Porchfest" {.image}



