---
title   : The Cully Jazz Festival (Switzerland)
date    : 2019-04-10
tags    : music, jazz
summary : |
  I saw Stacey Kent perform at the Cully Jazz Festival.
---

In April, I was visiting Lausanne, Switzerland for math-reasons -- I
was on the Ph.D. committee for a student of a colleague at the [EPFL].

Coincidentally, in nearby [Cully], the [37th Annual Cully Jazz
Festival] was taking place during my visit! The [village of Cully] is
on Lake Geneva just a quick train ride from Lausanne.

::::::::{.image-container-1col}
[![ref2]](/assets/images/2019-04--cully-switzerland.jpg)
::::::::

You can read more about the [history of the festival here].

A colleague and I went to the festival a couple of nights. On the
second, we saw British Jazz vocalist [Stacey Kent] perform in the
festival. It was a good show!

[EPFL]: https://www.epfl.ch/
[village of Cully]: https://en.wikipedia.org/wiki/Cully,_Switzerland
[Cully]: http://www.cully.ch/
[37th Annual Cully Jazz Festival]: https://blog.cullyjazz.ch/?format=gallery&annee=2019
[history of the festival here]: https://cullyjazz.ch/en/the-festival-s-portrait
[Stacey Kent]: https://www.staceykent.com/

::::::::{.image-container-2col}
[![ref0]](/assets/images/2019-04--Cully-jazz.jpg)

[![ref1]](/assets/images/2019-04--Stacey-Kent.png)
::::::::

[ref0]: /assets/images/2019-04--Cully-jazz.jpg "Cully Jazz Festival" {.image}
[ref1]: /assets/images/2019-04--Stacey-Kent.png "Stacey Kent" {.image}
[ref2]: /assets/images/2019-04--cully-switzerland.jpg "Cully, Switzerland" {.image}




        
