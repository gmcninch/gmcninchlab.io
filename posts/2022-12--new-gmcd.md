---
title: New GMCD 2022
date: 2022-12-27
tags: GMCD, music
summary: 
---

Around Christmas time, roughly each year since 2005, I've made a
"playlist" each year. I used to burn CDs and give them as presents to
friends and relatives, and I called them "GMCDs".

It appears that I missed making one in December 2021, but I made one
this year. See below for the content of the playlist.

This year instead of burning CDs, I uploaded the contents of the
playlist to [MixCloud](https://www.mixcloud.com). (Well, I also did
burn a few CDs...)

Find the [Mixcloud GMCD show here.](https://www.mixcloud.com/stencilv/gmcd-december-2022/)

You can find [all of the playlists I've made here](/pages/gmcds.html)


# gmcd-2022-12 
  #. Tiawa - *Mountains Of Metal*     
     From the album [Moonlit Train](https://tiawa-music.bandcamp.com/album/moonlit-train) 

  #. Ana Mazzotti - *Agora Ou Nunca Mais*     
     From the album [Ninguem Vai Me Segurar](https://anamazzotti.bandcamp.com/album/ninguem-vai-me-segurar) 

  #. Gretchen Parlato - *É Preciso Perdoar*     
     From the album [Flor](https://gretchenparlato.bandcamp.com/album/flor) 

  #. Cleo Sol - *Sunshine*     
     From the album [Mother](https://cleosol.bandcamp.com/album/rose-in-the-dark) 

  #. Roy Ayers with Adrian Younge and Ali Shaheed Muhammad - *Sunflowers*     
     From the album [Roy Ayers JID002](https://royayers.bandcamp.com/album/roy-ayers-jid002) 

  #. Submotion Orchestra - *All Yours*     
     From the album [Finest Hour](https://submotionorchestra.bandcamp.com/album/finest-hour) 

  #. The Souljazz Orchestra - *Sorrow Fly Away*     
     From the album [Under Burning Skies](https://thesouljazzorchestra.bandcamp.com/album/under-burning-skies) 

  #. The Dining Rooms - *Art Is A Cat (Feat. Beatrice Velasco Moreno)*     
     From the album [Art Is A Cat](https://thediningrooms.bandcamp.com/album/art-is-a-cat) 

  #. Beady Belle - *Last Dance*     
     From the album [Nothing But The Truth](https://beadybelle.bandcamp.com/album/nothing-but-the-truth) 

  #. Guts - *Já Não Há Mais Paz Feat. Catia Werneck*     
     From the album [Philantropiques](https://guts-puravida.bandcamp.com/album/philantropiques) 

  #. Natalie Duncan - *Sirens*     
     From the album [Free](https://natalieduncan.bandcamp.com/album/free) 

  #. Tiawa - *Can't Turn Back*     
     From the album [Moonlit Train](https://tiawa-music.bandcamp.com/album/moonlit-train) 



