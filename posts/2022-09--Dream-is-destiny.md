---
title: Dream is destiny
date: 2022-09-21
tags: personal
summary: Avatar
---

So I wanted an illustrated on-line avatar, but got tired of ones like this:

::: {.image-container-1col}
[![ref0]](/assets/images/myAvatar.png)
:::

[ref0]: /assets/images/myAvatar.png {.small-image}

Somehow I had the idea of using an image of the paper fortune teller
from the [roto-scoped](https://en.wikipedia.org/wiki/Rotoscoping) film
[Waking Life](https://en.wikipedia.org/wiki/Waking_Life)

::: {.image-container-1col}
[![ref1]](/assets/images/waking-life.jpg)
:::

[ref1]: /assets/images/waking-life.jpg {.small-image}

I remember
[paper fortune-tellers](https://en.wikipedia.org/wiki/Paper_fortune_teller)
from when I was a kid, of course. And the scene in which it appeared
in the film was great: [Dream is
Destiny](https://www.youtube.com/watch?v=GcSg-Mx-SRo).
