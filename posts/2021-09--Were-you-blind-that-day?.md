---
title: Were you blind that day?
date: 2021-09-01
tags: pop-music, jazz
summary: 
---

For a few years now, I've been a big fan of Brazilian jazz vocalist
[Luciana Souza](https://www.lucianasouza.com/) (see also
[wikipedia](https://en.wikipedia.org/wiki/Luciana_Souza)).

The other day, I happened to listen to the old [Steely
Dan](https://en.wikipedia.org/wiki/Steely_Dan) album
[Gaucho](https://en.wikipedia.org/wiki/Gaucho_(album)), and noticed
that the melody of *Third World Man* was somehow very familiar. As a
"kid", I didn't listen to that album *so* much, and *Third World Man*
wasn't really one of the "radio songs".  I'm sure I knew the tune, but
my familiarity seemed much more recent...

Eventually, I realized that *Third World Man* is the same tune as the
song *Were you blind that day?* on Luciana Souza's album [**The New
Bossa Nova**](https://www.lucianasouza.com/newbossanova) with
different lyrics.  [You can hear her tune
here](https://www.youtube.com/watch?v=KlmnxytRL44). I believe that is
[Chris Potter](https://www.chrispottermusic.com/) playing tenor sax in
the tune.  At any rate, I liked Souza's song enough to put it in one
of my yearly [*play lists*](../pages/gmcds.html) (if you click that
link, you'll find the tune in **gmcd-2013-12**).

:::::: {.image-container-1col}
[![ref0]](/assets/images/Luciana-Souza--The-New-Bossa-Nova.jpg)
::::::

[ref0]: /assets/images/Luciana-Souza--The-New-Bossa-Nova.jpg "The New Bossa Nova" {.image}


A good bit of [Luciana Souza's discography is available on bandcamp](https://lucianasouza.bandcamp.com/), but
apparently (?) not *The New Bossa Nova*.

Anyhow, back to the point. Some googling led to still more details.
This [reddit
thread](https://www.reddit.com/r/SteelyDan/comments/hzz4vq/were_you_blind_that_day/),
explains that:

- *Were you blind that day?* was originally a Steely Dan tune intended
  for their album **The Royal Scam**. For some reasons, plans change;
  meanwhile the song evolved to be *Third World Man* on the later
  **Gaucho** album.

- The tune with *Were you blind that day?* lyrics was never released
  on an album by Steely Dan, though it can be heard here:
  [out-takes](https://youtu.be/IfKK6e0BPAU).

- There is at least some sort of connection between Souza and Steely
  Dan.  E.g. Luciana Souza's album **The New Bossa Nova** was produced
  by her husband, [Larry
  Klein](https://en.wikipedia.org/wiki/Larry_Klein). And Klein worked
  with [Walter Becker](https://en.wikipedia.org/wiki/Walter_Becker)
  e.g. on Becker's 2008 solo album.

Anyhow, fun to learn this trivia. And: it is a beautiful tune!
