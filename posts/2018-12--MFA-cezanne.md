---
title: Very familiar...!
date: 2018-12-17
tags:  art, museums
summary: In which I encounter a very familiar piece of artwork...
---

When I was growing up, a print of this Cezanne painting [“A Turn in
the Road” (~1881)] hung on the wall in my family's living room. It is
so familiar to me that I was a bit startled to see it in the [Boston
MFA] during a recent visit!

[“A Turn in the Road” (~1881)]: https://collections.mfa.org/objects/33254 
[Boston MFA]: https://www.mfa.org/

::::::{.image-container-1col}
[![ref]](/assets/images/2018-12--MFA--Cezanne.jpg)
::::::


[ref]: /assets/images/2018-12--MFA--Cezanne.jpg "" {.image}

