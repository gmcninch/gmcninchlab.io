---
title: Tidal and Spotify links for this year's GMCD playlist(s)
date: 2024-12-20
tags: GMCD, music
summary: 
---

Here are the links for some recent playlists. You should pick the link
you want to use based on whether you use Tidal or spotify (the content
of the playlist on each site ought to be "the same").


- [2024-12 GMCD - tidal](https://tidal.com/browse/playlist/400e9abc-fe69-4e3b-aa97-11138f256f35)

- [2024-12 GMCD - spotify](https://open.spotify.com/playlist/5tQeyca6LrWzYtZwOzHzGX?si=cefce00cf0c4485b)

This was the first year that I really used streaming services for
music listening, and that is probably reflected in the 2024-12
playlist. I've spend a lot of time listening to Tidal playlists like
[UK
jazz](https://tidal.com/browse/playlist/88c9d318-f9b4-4485-87fd-71cc41b8c6c7)
and playlists corresponding to artists like
[Fatima](https://tidal.com/browse/artist/30738186?u) and others. I
discovered some new artists this way, including [Hiatus
Kaiyote](https://tidal.com/browse/artist/4811562?u). Some of these new
artists are represented in this playlist.

The 2024-12 GMCD playlist was my usual effort at making a playlist
during the holidays. But I actually also made a playlist last June:

- [2024-06 GMCD - tidal](https://tidal.com/browse/playlist/414c56f6-6565-4245-88c3-2c882a4a393a)

- [2024-06 GMCD - spotify](https://open.spotify.com/playlist/6t2Rp9aY3MPgELUUihWnqa?si=e552bbf99d744d1f)


The 2024-06 playlist features tracks from new albums by Alice Russell,
Elizabeth Shepherd, and Nicola Conte (see links below!) -- which I
mostly didn't include on the holiday playlist, because it is already
represented here!  -- as well as some songs that I was listening to a
lot early last summer.


New albums I've been excited about:

- [Alice Russell - I Am](https://alicerussellmusic.bandcamp.com/album/i-am)

- [Elizabeth Shepherd - Three Things](https://elizabethshepherd.bandcamp.com/album/three-things)

- [Nicola Conte - Umoja](https://nicolaconte.bandcamp.com/album/umoja)

I was traveling in November, including a visit to Grenoble, France. By
chance, Alice Russell was supposed to be playing in a club in Grenoble
*the day of my arrival there*. I was really excited ... but
unfortunately in the end she had to cancel that concert date. So,
unfortunately I didn't get to see her sing. Maybe someday...
