---
title: Novels and non-fiction 2022
date: 2022-11-11
tags: reading, novel, sci-fi, media
summary: 
---
**This post is part of the [media](2022-12--media.html) series**


# James S.A.Corey "Caliban's War"
  read-date: 2022-11-11
  
  novel, sci-fi

Caliban's War is the second novel of The Expanse series by James S.A. Corey

::: {.image-container-1col}
[![img1]](/assets/images/2022-11--Caliban's-War.jpg)
:::

[img1]: /assets/images/2022-11--Caliban's-War.jpg {.med-image}

The book series was made (or: is still being made?) into a [television
series](https://www.imdb.com/title/tt3230854/).

# Andy Weir - "Project Hail Mary"
  finished-date: 2022-12-12
  sci-fi, media

  "Project Hail Mary" was an enjoyable "hard sci-fi" novel (I actually
  listened to it via *audible*).

::: {.image-container-1col}
[![img2]](/assets/images/2022-12--Project_Hail_Mary,_First_Edition_Cover_(2021).jpg)
:::

[img2]: /assets/images/2022-12--Project_Hail_Mary,_First_Edition_Cover_(2021).jpg  {.med-image}
