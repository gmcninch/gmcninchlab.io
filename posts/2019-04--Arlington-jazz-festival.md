---
title   : 6th Annual Arlington Jazz Festival (April 2019)
date    : 2019-04-30
tags    : jazz, Arlington
summary : |
  The 6th Annual Arlington Jazz Festival, April 2019.
---

The [6th annual Arlington Jazz Festival] was an enjoyable event this
April.

The festival included performances by [The Yosveny Terry Quintet]
(see photo below) and others.

Thanks to the [festival's board of directors] for organizing this
event!

[6th annual Arlington Jazz Festival]: http://www.arlingtonjazz.org/
[The Yosveny Terry Quintet]: http://yosvanyterry.com/
[festival's board of directors]: http://www.arlingtonjazz.org/about-us.html

::::::::{.image-container-2col}
[![ref0]]( /assets/images/2019-04--Arlington-Jazz-Festival--Yosveny Terry.jpg)

[![ref1]]( /assets/images/2019-04--arlingtonjazz-festival.jpg)
::::::::

[ref0]: /assets/images/2019-04--Arlington-Jazz-Festival--Yosveny Terry.jpg "" {.image}
[ref1]: /assets/images/2019-04--arlingtonjazz-festival.jpg "" {.image}


  
