---
author: George McNinch
title : Snow shadows
date  : 2019-03-17
tags  : photography, nature
summary : |
  Photos of some interesting interaction between sunlight, shadows and snow-melt.
---

One weekend in March 2019, I was house- and pup-sitting for a few
nights in Western Massachusetts.

On Friday night it snowed lightly, and there was a good bit of sun the
next morning. I thought the contrast between snow-melt in the sun and
in the shade was somewhat striking. I took these two photos with my
phone.

:::::::::{.image-container-3col}
[![ref0]](/assets/images/2019-03-17--pup-sitting.jpg)

[![ref1]](/assets/images/2019-03--snow-and-shadows-1.jpg)

[![ref2]](/assets/images/2019-03--snow-and-shadows-2.jpg)
:::::::::

[ref0]: /assets/images/2019-03-17--pup-sitting.jpg "" {.image}
[ref1]: /assets/images/2019-03--snow-and-shadows-1.jpg "" {.image}
[ref2]: /assets/images/2019-03--snow-and-shadows-2.jpg "" {.image}

