---
title: The Cinematic Orchestra at the Paradise
date: 2019-11-07
tags: music 
summary: The Cinematic Orchestra performed in Boston at the Paradise Rock Club
---

:::::: {.image-container-1col}
[![ref0]](/assets/images/2019-11--TCO-tour-dates.jpg)
::::::

[ref0]: /assets/images/2019-11--TCO-tour-dates.jpg "Tour dates" {.image}
		   
	       
On Thursday November 7, I saw [The Cinematic Orchestra] perform in
the [Paradise Rock Club] in Boston.

I previously saw TCO play at the Paradise in 2007; that is longer ago
than it seems like it oughta be.
        
It was an enjoyable show!  You can see some images below; more are
[available here at vanyaland].

[The Cinematic Orchestra]: https://cinematicorchestra.com
[Paradise Rock Club]: http://paradiserock.club-boston.org 
[available here at vanyaland]: https://vanyaland.com/2019/11/08/photo-gallery-the-cinematic-orchestra-pbdy-and-photay-live-in-boston/

:::::::{.image-container-2col}
[![ref1]](/assets/images/2019-11--Cinematic-Orchestra13.jpg) 

[![ref2]](/assets/images/2019-11--Cinematic-Orchestra20.jpg)
:::::::

[ref1]: /assets/images/2019-11--Cinematic-Orchestra13.jpg "TCO" {.image}
[ref2]: /assets/images/2019-11--Cinematic-Orchestra20.jpg "TCO" {.image}
