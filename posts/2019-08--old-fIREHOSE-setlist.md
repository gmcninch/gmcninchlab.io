---
title    : fIREHOSE setlist (May 1993)
date     : 2019-10-23
tags     : Eugene, music
summary  : |
  How did I end up with the set list from a 1993 WOW hall performance
  by fIREHOSE?
---

I was recently reminded that I have the band's hand-written setlist
from a show in the early '90s by
[fIREHOSE](https://en.wikipedia.org/wiki/Firehose_(band)). After the
show, a friend of mine asked the band for it, and she then gave it to
me. It has been in a box of *stored stuff* in a closet for a while
now. Probably the last time I thought about it, scanners weren't
commonplace!! See below for a scan.

Anyhow, I saw this show at the [WOW (Woodmen of the World)
hall](http://www.wowhall.org/) in Eugene, Oregon. At the time, I was a
graduate student at the [University of
Oregon](http://math.uoregon.edu), in Eugene. By my memory, the show
occurred in the spring before I got married, so 1993 is surely the correct
year.

Someone -- presumably Mike
@[wattfrompedro](https://twitter.com/wattfrompedro) (??) -- seems to
have written the setlist on the back of some scratch paper from the
WOW hall (namely, an order to a copy shop in Eugene which appears to
be unrelated to their gig...); indications on the back of the setlist
suggest that the show was in May.

A fair amount of time has gone by since then, but I remember enjoying
the music!!

Keep [Jamming Econo](http://www.mikewatt.com/)...

::::::::{.image-container-2col}
[![ref0]](/assets/images/1993-spring-fIREHOSE-setlist-for-WOW-hall-performance-p1.jpg)

[![ref1]](/assets/images/1993-spring-fIREHOSE-setlist-for-WOW-hall-performance-p2.jpg)
::::::::

[ref0]: /assets/images/1993-spring-fIREHOSE-setlist-for-WOW-hall-performance-p1.jpg "" {.image}
[ref1]: /assets/images/1993-spring-fIREHOSE-setlist-for-WOW-hall-performance-p2.jpg "" {.image}



		     
