---
title: Cassette Tape covers
author: George McNinch
date: 2019-10-25
tags: scans, music, memorabilia
summary: Scans of the hand-written track-lists of some of my cassette tapes from the 80s and 90s.
---

This collection represents *some* of the home-made cassette tapes that
I made/received/carried around in the '80s and first half of the
'90s. I got a CD player sometime during college (maybe 1988?)  and I
basically quit buying commercial cassette tapes at that point. (I
recall re-purchasing a fair number of albums on CD that I already
owned on cassette).
      
But cassettes were still great for recording **mixes** for friends,
and for myself. Moreover, I didn't have a CD player in my car until
the mid-to-late 90s. Until then, road trips always involved a bunch of
cassettes tapes.
     
Around 2011 I was organizing stuff in the house; while doing so I came
to the realization that I would surely never play my cassette tapes
again. I no longer even had a working cassette player (an observation
that would have scandalized 18-year-old me). The tapes were all 20+
years old, and many of them were likely failing (e.g. I recall that
the glue holding little pads guiding the tape tended to weaken
with age and heat, and the pads would fall off....) So I scanned the
hand-written covers of tapes that were still around and tossed the
tapes. I remember feeling a bit sad about it...

See below for the scans!


::::::::{.image-container-2col}
[![img0]](/assets/images/2011-03--Cassette-cover-scans/5ae7a948-0648-11ea-bb30-c35433fc685e.jpg)

[![img1]](/assets/images/2011-03--Cassette-cover-scans/5ae8dbf6-0648-11ea-8fa9-dbddac92c0e2.jpg)

[![img2]](/assets/images/2011-03--Cassette-cover-scans/5ae9ee06-0648-11ea-887e-131cc19f3f77.jpg)

[![img3]](/assets/images/2011-03--Cassette-cover-scans/5af1a40c-0648-11ea-97da-27dc99042a5f.jpg)

[![img4]](/assets/images/2011-03--Cassette-cover-scans/5af25e10-0648-11ea-82d6-f38f9f59216e.jpg)

[![img5]](/assets/images/2011-03--Cassette-cover-scans/5af3f07c-0648-11ea-bca2-abab673356d7.jpg)

[![img6]](/assets/images/2011-03--Cassette-cover-scans/5af4bd40-0648-11ea-905e-c7dc3a0dd972.jpg)

[![img7]](/assets/images/2011-03--Cassette-cover-scans/5af587ac-0648-11ea-86d1-b7434b799855.jpg)

[![img8]](/assets/images/2011-03--Cassette-cover-scans/5af64b88-0648-11ea-8695-abceb39a3d42.jpg)

[![img9]](/assets/images/2011-03--Cassette-cover-scans/5af70b2c-0648-11ea-af3c-7347475b9332.jpg)

[![img10]](/assets/images/2011-03--Cassette-cover-scans/5af7d1f6-0648-11ea-9e1e-53487c1d3f08.jpg)

[![img11]](/assets/images/2011-03--Cassette-cover-scans/5af88272-0648-11ea-aba9-575d350361f9.jpg)

[![img12]](/assets/images/2011-03--Cassette-cover-scans/5afaddf6-0648-11ea-a02a-87286a41fa6a.jpg)

[![img13]](/assets/images/2011-03--Cassette-cover-scans/5afbc0fe-0648-11ea-afad-c3b391e9e83d.jpg)

[![img14]](/assets/images/2011-03--Cassette-cover-scans/5afc76ca-0648-11ea-980b-1f59109488ff.jpg)

[![img15]](/assets/images/2011-03--Cassette-cover-scans/5afd13c8-0648-11ea-a4b9-df416a92cbd1.jpg)

[![img16]](/assets/images/2011-03--Cassette-cover-scans/5afdbe54-0648-11ea-9f44-cbe2af436a04.jpg)

[![img17]](/assets/images/2011-03--Cassette-cover-scans/5afe6cfa-0648-11ea-b050-331ca28bc088.jpg)

[![img18]](/assets/images/2011-03--Cassette-cover-scans/5aff1c7c-0648-11ea-a36f-7bb9a63b20b2.jpg)

[![img19]](/assets/images/2011-03--Cassette-cover-scans/5affd04a-0648-11ea-b5d0-e7794bc3b9a9.jpg)

[![img20]](/assets/images/2011-03--Cassette-cover-scans/5b020572-0648-11ea-93c9-bbe8d4d1bebd.jpg)

[![img21]](/assets/images/2011-03--Cassette-cover-scans/5b02edfc-0648-11ea-b151-9febc635d164.jpg)

<!-- [![img22]](/assets/images/2011-03--Cassette-cover-scans/5aeadfe6-0648-11ea-b471-67aecb7d01ff.jpg) -->

<!-- [![img23]](/assets/images/2011-03--Cassette-cover-scans/5af08306-0648-11ea-9b34-93114ba850df.jpg) -->
::::::::

[img0]: /assets/images/2011-03--Cassette-cover-scans/5ae7a948-0648-11ea-bb30-c35433fc685e.jpg {.image}
[img1]: /assets/images/2011-03--Cassette-cover-scans/5ae8dbf6-0648-11ea-8fa9-dbddac92c0e2.jpg {.image}
[img2]: /assets/images/2011-03--Cassette-cover-scans/5ae9ee06-0648-11ea-887e-131cc19f3f77.jpg {.image}
[img3]: /assets/images/2011-03--Cassette-cover-scans/5af1a40c-0648-11ea-97da-27dc99042a5f.jpg {.image}
[img4]: /assets/images/2011-03--Cassette-cover-scans/5af25e10-0648-11ea-82d6-f38f9f59216e.jpg {.image}
[img5]: /assets/images/2011-03--Cassette-cover-scans/5af3f07c-0648-11ea-bca2-abab673356d7.jpg {.image}
[img6]: /assets/images/2011-03--Cassette-cover-scans/5af4bd40-0648-11ea-905e-c7dc3a0dd972.jpg {.image}
[img7]: /assets/images/2011-03--Cassette-cover-scans/5af587ac-0648-11ea-86d1-b7434b799855.jpg {.image}
[img8]: /assets/images/2011-03--Cassette-cover-scans/5af64b88-0648-11ea-8695-abceb39a3d42.jpg {.image}
[img9]: /assets/images/2011-03--Cassette-cover-scans/5af70b2c-0648-11ea-af3c-7347475b9332.jpg {.image}
[img10]: /assets/images/2011-03--Cassette-cover-scans/5af7d1f6-0648-11ea-9e1e-53487c1d3f08.jpg {.image}
[img11]: /assets/images/2011-03--Cassette-cover-scans/5af88272-0648-11ea-aba9-575d350361f9.jpg {.image}
[img12]: /assets/images/2011-03--Cassette-cover-scans/5afaddf6-0648-11ea-a02a-87286a41fa6a.jpg {.image}
[img13]: /assets/images/2011-03--Cassette-cover-scans/5afbc0fe-0648-11ea-afad-c3b391e9e83d.jpg {.image}
[img14]: /assets/images/2011-03--Cassette-cover-scans/5afc76ca-0648-11ea-980b-1f59109488ff.jpg {.image}
[img15]: /assets/images/2011-03--Cassette-cover-scans/5afd13c8-0648-11ea-a4b9-df416a92cbd1.jpg {.image}
[img16]: /assets/images/2011-03--Cassette-cover-scans/5afdbe54-0648-11ea-9f44-cbe2af436a04.jpg {.image}
[img17]: /assets/images/2011-03--Cassette-cover-scans/5afe6cfa-0648-11ea-b050-331ca28bc088.jpg {.image}
[img18]: /assets/images/2011-03--Cassette-cover-scans/5aff1c7c-0648-11ea-a36f-7bb9a63b20b2.jpg {.image}
[img19]: /assets/images/2011-03--Cassette-cover-scans/5affd04a-0648-11ea-b5d0-e7794bc3b9a9.jpg {.image}
[img20]: /assets/images/2011-03--Cassette-cover-scans/5b020572-0648-11ea-93c9-bbe8d4d1bebd.jpg {.image}
[img21]: /assets/images/2011-03--Cassette-cover-scans/5b02edfc-0648-11ea-b151-9febc635d164.jpg {.image}
[img22]: /assets/images/2011-03--Cassette-cover-scans/5aeadfe6-0648-11ea-b471-67ABC7d01ff.jpg {.image}
[img23]: /assets/images/2011-03--Cassette-cover-scans/5af08306-0648-11ea-9b34-93114BA850df.jpg {.image}

	       
