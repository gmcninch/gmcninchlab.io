---
title: David Byrne's American Utopia
date: 2019-09-27
tags: music
summary: David Byrne's American Utopia performed in Boston, Massachusetts
---

I saw [David Byrne's American
Utopia](https://americanutopiabroadway.com/) perform in the [Emerson
Colonial Theatre](http://www.emersoncolonialtheatre.com) in Boston, MA.

The Boston shows happened just before the show went to Broadway. I
thought it was a very good show. It was a large-ish ensemble. And
impressively, everything was **wireless**, which made choreography for
the performers possible.

They performed more Talking Heads songs than I expected - for example,
they did a version of *Born Under Punches*!!

By the way, if you haven't seen it, you should watch this old video of
Talking Heads perform that song: [Born Under Punches in Rome, Italy
(1980)!!](https://www.youtube.com/watch?v=YO7N2tFb0X8).

::::::::{.image-container-1col}
[![ref]](/assets/images/2019-09--American-Utopia.png)

::::::::
[ref]: /assets/images/2019-09--American-Utopia.png "" {.image}
